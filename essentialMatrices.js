'use strict';

var fs = require('fs-extra');
var cv = require('opencv');
var keypoints = require('./keypoints');
var matrixOps = require('./matrixOps');

function findEssentialMatrix(firstImgPts, secondImgPts) {
    //Find the essential matrix
    //pass it firstImgPts,secondImgPts,algorithm,ransacMaxDist,and ransacProb
    var i;
    var decomposedMats;
    var essentialMask = [];
    var essentialMatrix;
    for (i = 0; i < firstImgPts.length; i += 1) {
        essentialMask.push({
            notMasked: 1
        });
    }
    if (firstImgPts.length && secondImgPts.length && firstImgPts.length >= 5) {

        //TODO Parameter 5 is currently not being passed in because of a stupid float to double conversion
        var mat1 = new cv.Matrix();
        var essentialRet = mat1.findEssentialMatrix(firstImgPts, secondImgPts, 'CV_FM_RANSAC', 1, 0.99, FOCAL_LENGTH, CAM_CENTER_X, CAM_CENTER_Y);
        essentialMatrix = essentialRet.essentialMat;
        if (essentialMatrix.size()[0] > 3) {
            console.log('Hit a weird multisized essential matrix on 1st pass');
            matrixOps.printMatrix(essentialMatrix, 'essentialMatrix');
            matrixOps.chopMatrix(essentialMatrix);
            matrixOps.printMatrix(essentialMatrix, 'essentialMatrix');
        }
        essentialMask = essentialRet.masks;
        /*
		var i;
		var points1Filtered = [];
		var points2Filtered = [];
		for(i = 0; i<firstImgPts.length; i+=1){
			if(essentialMask[i].notMasked){
				points1Filtered.push(firstImgPts[i]);
				points2Filtered.push(secondImgPts[i]);
			}
		}
		if(points1Filtered.length>=5){
			essentialRet = mat1.findEssentialMatrix(points1Filtered,points2Filtered,'CV_FM_RANSAC',3,0.99,FOCAL_LENGTH,CAM_CENTER_X,CAM_CENTER_Y);
			esentialMatrix = essentialRet.essentialMat;
			if(essentialMatrix.size()[0]>3){
				console.log('Hit a weird multisized essential matrix on 2nd pass');
				chopMatrix(essentialMatrix);
			}
			var copyEssentialMask = [];
			var x = 0;
			var i = 0;
			while(x<firstImgPts.length){
				if(essentialMask[x].notMasked){
					if(essentialRet.masks[i].notMasked){
						copyEssentialMask[x] = {notMasked: 1};
					}
					else{
						copyEssentialMask[x] = {notMasked: 0};
					}
					i+=1;
				}
				else{
					copyEssentialMask[x] = {notMasked: 0};
				}
				x+=1;
			}
			essentialMask = copyEssentialMask;
		}
		else{
			console.log('Not enough for a second RANSAC pass...');
		}
        */
        /*
        //Stupid hack for passing in the type as 20=CV_64FC1
        var cameraMatrix = new cv.Matrix(3,3,20);
        cameraMatrix.set(0,0,1564.18);
        cameraMatrix.set(0,1,0);
        cameraMatrix.set(0,2,962.02);
        cameraMatrix.set(1,0,0);
        cameraMatrix.set(1,1,1563.78);
        cameraMatrix.set(1,2,565.33);
        cameraMatrix.set(2,0,0);
        cameraMatrix.set(2,1,0);
        cameraMatrix.set(2,2,1);
        var cameraMatrixTrans = cameraMatrix.transpose(cameraMatrix);
        */
        //Multiply cameraMatrix-transpose by fundamental matrix by camera matrix to get the essential matrix
        //var essentialMatrix = fundamentalMatrix;// mat1.multiply((mat1.multiply(cameraMatrixTrans,fundamentalMatrix)),cameraMatrix);
        decomposedMats = essentialMatrix
            .recoverPose(essentialMatrix, firstImgPts, secondImgPts,
                essentialMask, FOCAL_LENGTH, CAM_CENTER_X, CAM_CENTER_Y);
        matrixOps.printMatrix(essentialMatrix, "essential matrix");
        //printMatrix(decomposedMats.transMat,"t");
    } else {
        console.log("<5 match points found!");
    }
    console.log("FIX THIS: Using just the translation matrix");
    matrixOps.printMatrix(decomposedMats.transMat, "trans mat");
    matrixOps.printMatrix(decomposedMats.rotMat, "rot mat");
    var fullTransMat = new cv.Matrix.Eye(3, 3);
    fullTransMat.set(0, 2, decomposedMats.transMat.get(0, 0));
    fullTransMat.set(1, 2, decomposedMats.transMat.get(1, 0));
    fullTransMat.set(2, 2, 1 + decomposedMats.transMat.get(2, 0));
    var compositeMat = fullTransMat.multiply(fullTransMat, decomposedMats.rotMat);
    //decomposedMats.transMat = decomposedMats.transMat.multiply(decomposedMats.transMat,decomposedMats.rotMat);
    return {
        essentialMask: essentialMask,
        transMat: decomposedMats.transMat,
        rotMat: decomposedMats.rotMat,
        essentialMatrix: essentialMatrix
    };
}

//So basically, we need to find matches between keypoints in image 1 and 2
//that then also match between image 2 and 3. These points are "special",
//and will help us determine the actual translation vector. We should compute the
//3d points for all of these using 1->2 and 2->3 and assumed translation vectors. Then,
//we can use the idea that the actual 3d points must be equal to say that
//pt1.x(1->2) = xRel*pt1.x(2->3) , so pt1.x(1->)/pt1.x(2->3) = xRel. Same reasoning for y and z.
//Then, the "real" translation becomes all keypoints in (2->3) multiplied by xRel,yRel,and zRel.
//Also, maybe we average the xRel,yRel, and zRel for all of the points matching in 1,2, and 3 to
//get a composite value???
function makeEssentialMatrices(filePath, callback) {

    //Read the info file for the google drive keypoints folder to figure out what the matches are
    var keypointsInfo = JSON.parse(fs.readFileSync(filePath + KEYPOINTS_FOLDER + "\\" + KEYPOINTS_INFO_FILE));
    try {

        //Wipe the essential matrices folder if it exists
        fs.statSync(filePath + ESSENTIAL_MATRICES_FOLDER).isDirectory();
        fs.removeSync(filePath + ESSENTIAL_MATRICES_FOLDER);
    } catch (err) {

    }

    //Make the essential matrices folder
    fs.mkdirSync(filePath + ESSENTIAL_MATRICES_FOLDER);
    var i, curMatches;
    //Find the essential matrices for each chain keypoint set
    if (keypointsInfo.chainFiles) {
        for (i = 0; i < keypointsInfo.chainFiles.length; i += 1) {
            curMatches = JSON.parse(fs.readFileSync(filePath + KEYPOINTS_FOLDER + '\\' + keypointsInfo.matchNames + keypointsInfo.chainFiles[i].filename));
            findMatrix(curMatches, filePath + ESSENTIAL_MATRICES_FOLDER + '\\' + ESSENTIAL_MATRIX_FILE + keypointsInfo.chainFiles[i].filename);
        }
    }
    //Find the essential matrices for each scale keypoint set
    if (keypointsInfo.scaleFiles) {
        for (i = 0; i < keypointsInfo.scaleFiles.length; i += 1) {
            curMatches = JSON.parse(fs.readFileSync(filePath + KEYPOINTS_FOLDER + '\\' + keypointsInfo.matchNames + keypointsInfo.scaleFiles[i].filename));
            findMatrix(curMatches, filePath + ESSENTIAL_MATRICES_FOLDER + '\\' + ESSENTIAL_MATRIX_FILE + keypointsInfo.scaleFiles[i].filename);
        }
    }
    fs.writeFileSync(filePath + ESSENTIAL_MATRICES_FOLDER + '\\' + ESSENTIAL_MATRIX_INFO_FILE, JSON.stringify({
        chainMatrices: (keypointsInfo.chainFiles) ? keypointsInfo.chainFiles.length : 0,
        scaleMatrices: (keypointsInfo.scaleFiles) ? keypointsInfo.scaleFiles.length : 0,
        essentialMatrixNames: ESSENTIAL_MATRIX_FILE
    }));
    console.log('Made all essential matrices');
    callback();
}

function findMatrix(curMatches, filePath) {
    var allImgPts = keypoints.getKeypoints(curMatches);
    var essentialMatrix = findEssentialMatrix(allImgPts.firstImgPts, allImgPts.secondImgPts);
    //checkEpiConstraint(allImgPts.firstImgPts,allImgPts.secondImgPts,essentialMatrix.essentialMatrix);
    var transMat = matrixOps.printMatrix(essentialMatrix.transMat, filePath + " translation");
    var rotMat = matrixOps.printMatrix(essentialMatrix.rotMat, filePath + " rotation1");
    fs.writeFileSync(filePath, JSON.stringify({
        translation: transMat,
        rotation: rotMat
    }));
}

module.exports.findEssentialMatrix = findEssentialMatrix;
module.exports.makeEssentialMatrices = makeEssentialMatrices;