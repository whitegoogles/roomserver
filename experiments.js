'use strict';
var fs = require('fs-extra');
var Canvas = require('canvas');
var matrixOps = require('./matrixOps');
var essentialMatrix = require('./essentialMatrices');
var model = require('./model');
var frames = require('./frames');
var keypoints = require('./keypoints');
var cv = require('opencv');

function moveImgOnPath(path, fileName, folder) {
    var i;
    var imgData = fs.readFileSync(folder + "/" + fileName);
    var img = new Canvas.Image();
    img.src = imgData;
    var maxX = -1;
    var maxY = -1;
    for (i = 0; i < path.length; i += 1) {
        if (path[i].x + img.width > maxX) {
            maxX = path[i].x + img.width;
        }
        if (path[i].y + img.height > maxY) {
            maxY = path[i].y + img.height;
        }
    }
    for (i = 0; i < path.length; i += 1) {
        var cnv = new Canvas(1080, 1920);
        var ctx = cnv.getContext('2d');
        ctx.drawImage(img, path[i].x, path[i].y, img.width, img.height);
        fs.writeFileSync(folder + "/" + IMAGES_FOLDER + "/" + IMAGE_FILE.split(".")[0] + "_" + (i + 1) + ".png", cnv.toBuffer());
    }
}

function newRelativeMotionExperiment() {
    var i, path;
    path = [];
    for (i = 0; i < 10; i += 1) {
        path.push({
            x: i * 20,
            y: i * 20
        });
    }
    for (i = 0; i < 5; i += 1) {
        path.push({
            x: i * 20,
            y: 180 - i * 20
        });
    }
    moveImgOnPath(path, "bird.png", CACHE_FOLDER + "/test-x");
}

function relativeMotionExperiment() {
    matrixOps.printMatrix(essentialMatrix.findEssentialMatrix(
        [{
            x: 128.02175903320312,
            y: 15.862680435180664
        }, {
            x: 101.87322998046875,
            y: 18.13946533203125
        }, {
            x: 30,
            y: 20,
            z: 1
        }, {
            x: 20,
            y: 20,
            z: 1
        }, {
            x: 20,
            y: 10,
            z: 1
        }],
        [{
            x: 20,
            y: 30,
            z: 1
        }, {
            x: 10,
            y: 30,
            z: 1
        }, {
            x: 20,
            y: 20,
            z: 1
        }, {
            x: 10,
            y: 20,
            z: 1
        }, {
            x: 10,
            y: 10,
            z: 1
        }]
    ).transMat, "translation by 10 units");

    matrixOps.printMatrix(essentialMatrix.findEssentialMatrix(
        [{
            x: 60,
            y: 30,
            z: 1
        }, {
            x: 50,
            y: 30,
            z: 1
        }, {
            x: 60,
            y: 20,
            z: 1
        }, {
            x: 50,
            y: 20,
            z: 1
        }, {
            x: 50,
            y: 10,
            z: 1
        }, {
            x: 60,
            y: 10,
            z: 1
        }],
        [{
            x: 30,
            y: 30,
            z: 1
        }, {
            x: 20,
            y: 30,
            z: 1
        }, {
            x: 30,
            y: 20,
            z: 1
        }, {
            x: 20,
            y: 20,
            z: 1
        }, {
            x: 20,
            y: 10,
            z: 1
        }, {
            x: 30,
            y: 10,
            z: 1
        }]
    ).transMat, "translation by 40 units");

    matrixOps.printMatrix(essentialMatrix.findEssentialMatrix(
        [{
            x: 60,
            y: 30,
            z: 1
        }, {
            x: 50,
            y: 30,
            z: 1
        }, {
            x: 60,
            y: 20,
            z: 1
        }, {
            x: 50,
            y: 20,
            z: 1
        }, {
            x: 50,
            y: 10,
            z: 1
        }, {
            x: 60,
            y: 10,
            z: 1
        }],
        [{
            x: 20,
            y: 30,
            z: 1
        }, {
            x: 10,
            y: 30,
            z: 1
        }, {
            x: 20,
            y: 20,
            z: 1
        }, {
            x: 10,
            y: 20,
            z: 1
        }, {
            x: 10,
            y: 10,
            z: 1
        }, {
            x: 20,
            y: 10,
            z: 1
        }]
    ).transMat, "translation by 50 units");
}
/*
function makeSingleModels(){
    var modelsDir = "www\\experiments\\single-models";
    var modelsBaseDir = "single-models-base";
    var copyFromFolder = 
    try{
        fs.statSync(modelsDir);
        fs.removeSync(modelsDir);
    }
    fs.mkdirSync(modelsDir);
    function makeModelIter(i){
        model.makeModel(modelsBaseDir,function(){
            if(i<18){
                i+=1;
                fs.mkdirSync(modelsDir+"\\model"+i+".ply");
                makeModelIter(i);
            }
        });
    }
    
}*/

//Angle in degrees
var AXIS = {
    x: 1,
    y: 2,
    z: 3
}
function getRotation(angle,axis){
    var rotMat = new cv.Matrix.Eye(3,3);
    var cosAngle = Math.cos(angle*(Math.PI/180));
    var sinAngle = Math.sin(angle*(Math.PI/180));
    switch(axis){
        case AXIS.x:
            rotMat.set(1,1,cosAngle);
            rotMat.set(1,2,-sinAngle);
            rotMat.set(2,1,sinAngle);
            rotMat.set(2,2,cosAngle);
            break;
        case AXIS.y:
            rotMat.set(0,0,cosAngle);
            rotMat.set(0,2,sinAngle);
            rotMat.set(2,0,-sinAngle);
            rotMat.set(2,2,cosAngle);
            break;
        case AXIS.z:
            rotMat.set(0,0,cosAngle);
            rotMat.set(0,1,-sinAngle);
            rotMat.set(1,0,sinAngle);
            rotMat.set(1,1,cosAngle);
            break;
    }
    return rotMat;
}

function getPixel(worldPoint,transX,transY,transZ,rotMat){
    //x = K[R|t]X
    var rotTransMat = new cv.Matrix.Eye(4,4);
    if(rotMat){
        var i,j;
        for(i = 0; i<3; i+=1){
            for(j = 0; j<3; j+=1){
                rotTransMat.set(i,j,rotMat.get(i,j));
            }
        }
    }
    worldPoint.set(3,0,1);
    rotTransMat.set(0,3,transX);
    rotTransMat.set(1,3,transY);
    rotTransMat.set(2,3,transZ);
    var cameraMat = model.getCameraMatrix();
    var movedMat = rotTransMat.multiply(rotTransMat,worldPoint);
    var flattenedMat = cameraMat.multiply(cameraMat,movedMat);
    var pixel = {
        x:flattenedMat.get(0,0)/flattenedMat.get(2,0),
        y:flattenedMat.get(1,0)/flattenedMat.get(2,0)
    }
    //matrixOps.printMatrix(worldPoint,"worldPoint");
    console.log(JSON.stringify(pixel));
    console.log(transZ);
    /*if(global.GLOBAL_WORLD_PT[0].get(0,0) === worldPoint.get(0,0) || global.GLOBAL_WORLD_PT[1].get(0,0) === worldPoint.get(0,0)){
        matrixOps.printMatrix(worldPoint,"world point");    
        console.log(JSON.stringify(pixel));
        matrixOps.printMatrix(rotTransMat,"rot trans mat");
        matrixOps.printMatrix(cameraMat,"cameraMat");
        matrixOps.printMatrix(model.getWorldPoint(pixel,new cv.Matrix.Eye(4,4),cameraMat.inverse(cameraMat)),"reprojected world point");
    }*/
    cameraMat.release();
    rotTransMat.release();
    movedMat.release();
    flattenedMat.release();
    return pixel;
}

function getPixels(worldPoints,transX,transY,transZ,rotMat){
    var pixels = [];
    var i;
    for(i = 0;i<worldPoints.length; i+=1){
        var pixel = getPixel(worldPoints[i].worldPoint,transX,transY,transZ,rotMat);
        pixel.color = worldPoints[i].color;
        pixels.push(pixel);
        /*if(i === 3 || i == 4){
            global.PIXEL_PTS.push(pixel);
        }*/
    }
    return pixels;
}

function genCube(){
    var worldPoints = [];
    var i;
    for(i = 0; i<8; i+=1){
        worldPoints.push({
            worldPoint:new cv.Matrix.Eye(4,1),
            color:{
                red:Math.floor(Math.random()*255),
                blue:Math.floor(Math.random()*255),
                green:Math.floor(Math.random()*255)
            }
        });
    }
    setColumn(worldPoints[0].worldPoint,[0.25,0,2]);
    setColumn(worldPoints[1].worldPoint,[0.5,0,2]);
    setColumn(worldPoints[2].worldPoint,[0.5,0.25,2]);
    setColumn(worldPoints[3].worldPoint,[0.25,0.25,2]);
    setColumn(worldPoints[4].worldPoint,[0.25,0,3]);
    setColumn(worldPoints[5].worldPoint,[0.5,0,3]);
    setColumn(worldPoints[6].worldPoint,[0.5,0.25,3]);
    setColumn(worldPoints[7].worldPoint,[0.25,0.25,3]);
    return worldPoints;
}

function setColumn(mat,array){
    var i;
    for(i = 0; i<array.length; i+=1){
        mat.set(i,0,array[i]);
    }
}

function genRandomPoints(numPoints,bounds,shiftX,shiftY){
    var worldPoints = [];
    var i;
    global.GLOBAL_WORLD_PT = [];
    for(i = 0; i<numPoints; i+=1){
        var worldPoint = new cv.Matrix.Eye(4,1);
        worldPoint.set(0,0,Math.random()); //Math.random()*bounds-shiftX
        worldPoint.set(1,0,Math.random()); //Math.random()*bounds-shiftY
        worldPoint.set(2,0,i+1); //Math.random()*bounds
        /*if(i == 3 || i == 4){
            if(i == 3){
                worldPoint.set(0,0,1);
                worldPoint.set(1,0,0);
                worldPoint.set(2,0,3);
            }
            if(i==4){
                worldPoint.set(0,0,3);
                worldPoint.set(1,0,0);
                worldPoint.set(2,0,3);
            }
            global.GLOBAL_WORLD_PT.push(worldPoint);
            global.PIXEL_PTS = [];
        }*/
        /*if(i == numPoints-1){
            worldPoint.set(2,0,1500);
        }
        if(i == 0){
            worldPoint.set(2,0,0);
        }*/
        worldPoint.set(3,0,1);
        worldPoints.push({
            worldPoint:worldPoint,
            color:{
                red:Math.floor(Math.random()*255),
                blue:Math.floor(Math.random()*255),
                green:Math.floor(Math.random()*255)
            }
        });
    }
    return worldPoints;
}

function writePixelsToImage(filename,pixels){
    var cnv = new Canvas(1080,1920);
    var ctx = cnv.getContext("2d");
    ctx.fillStyle = "#FFFFFF";
    ctx.fillRect(0,0,cnv.width,cnv.height);
    var i;
    for(i = 0; i<pixels.length; i+=1){
        ctx.fillStyle = "rgba("+pixels[i].color.red+","+pixels[i].color.green+","+pixels[i].color.blue+",1)";
        //ctx.fillStyle = "#000000";
        ctx.fillRect(pixels[i].x,pixels[i].y,20,20);
        if(pixels[i].x>cnv.width || pixels[i].y>cnv.height || pixels[i].x<0 || pixels[i].y<0){
            console.log("Point was out of bounds "+JSON.stringify(pixels[i]));
        }
    }
    fs.writeFileSync(filename,cnv.toBuffer());
}

function realExperiment(){
    global.MAX_MODEL_ITERATIONS = 2;
    global.REGION_SIZE = 30;
    global.MAX_MATCHES = 400;
    global.SIFT_LAST_IMG = 5;
    var experimentPath = CACHE_FOLDER+"\\testThreeShirt\\";
    autoProcessFolder(experimentPath);
}

function fiveShirtExperiment(){
    global.MAX_MODEL_ITERATIONS = 10;
    global.REGION_SIZE = 30;
    global.MAX_MATCHES = 400;
    global.SIFT_LAST_IMG = 5;
    var experimentPath = CACHE_FOLDER+"\\testFiveShirt\\";
    autoProcessFolder(experimentPath);
}

function xExperiment(){
    global.MAX_MODEL_ITERATIONS = 10;
    global.REGION_SIZE = 30;
    global.MAX_MATCHES = 400;
    global.SIFT_LAST_IMG = 5;
    var experimentPath = CACHE_FOLDER+"\\0B_Omro6mIy3sSExWeE1WQjF3dEE\\";
    autoProcessFolder(experimentPath);
}

function zoomExperiment(){
    global.SIFT_LAST_IMG = 5;
    global.REGION_SIZE = 15;
    global.MAX_MODEL_ITERATIONS = 10;
    var experimentPath = CACHE_FOLDER+"\\test-x\\";
    autoProcessFolder(experimentPath);
}

function chainExperiment(){
    global.MAX_MODEL_ITERATIONS = 40;
    global.REGION_SIZE = 15;
    global.MAX_MATCHES = 400;
    var experimentPath = CACHE_FOLDER+"\\testFiveShirt\\";
    keypoints.makeSiftChain(experimentPath, function () {
        essentialMatrix.makeEssentialMatrices(experimentPath, function () {
            //writePureMotionMatrices(experimentPath,0,0,1);
            model.makeModel(experimentPath, function () {
                console.log('Finished chain experiment');
            });
        });
    }); 
}

function autoProcessFolder(experimentPath,callback){
    keypoints.makeSiftMatches(experimentPath, function () {
        essentialMatrix.makeEssentialMatrices(experimentPath, function () {
            //writePureMotionMatrices(experimentPath,0,0,1);
            model.makeModel(experimentPath, function () {
                console.log('Finished making the model');
                if(callback){
                    callback();
                }
            });
        });
    });
}

//Move by 1 in the x and 2 in the z and 1 in the y
//Rotate by 40 degrees on the z axis
//Move by 2 in the x and 3 in the z and 0 in the y
//Rotate by 20 degrees on the y axis
function cubeRotationExperiment(){
    var cubePoints = genCube();
    var pixels1 = cubePoints;
    var pixels2 = getPixels(cubePoints,1,1,2,getRotation(40,AXIS.z));
    var pixels3 = getPixels(cubePoints,3,5,1,getRotation());
}

function cubeExperiment(){
    MAX_MODEL_ITERATIONS = 4;
    global.REGION_SIZE = 1;
    global.MAX_MATCHES = 400;
    global.SIFT_LAST_IMG = 5;
    console.log("This won't work because the points it projects aren't in the images provided for the base. ");
    var experimentPath = CACHE_FOLDER;
    var randPoints = genCube();//genRandomPoints(50,0.5,CAM_CENTER_X/FOCAL_LENGTH,CAM_CENTER_Y/FOCAL_LENGTH);
    var pixels1 = getPixels(randPoints,0,0,1);
    var pixels2 = getPixels(randPoints,0,0,2);
    console.log(JSON.stringify(pixels1));
    console.log(JSON.stringify(pixels2));
    var pixels3 = getPixels(randPoints,0,0,3);
    var pixels4 = getPixels(randPoints,0,0,4);
    var outputDir = experimentPath+"\\synthetic-images";
    var baseDir = experimentPath+"\\0B_Omro6mIy3scjYwTndteGt2eXM";
    try{
        fs.statSync(outputDir);
        fs.removeSync(outputDir);
    }
    catch(err){
        
    }
    fs.mkdirSync(outputDir);
    fs.copySync(baseDir,outputDir);
    var titleFile = JSON.parse(fs.readFileSync(outputDir+"\\"+TITLE_FILE));
    titleFile.fileName = "Synthetic Images Test";
    fs.writeFileSync(outputDir+"\\"+TITLE_FILE,JSON.stringify(titleFile));
    var framesFile = JSON.parse(fs.readFileSync(outputDir+"\\"+IMAGES_FOLDER+"\\"+IMAGE_INFO_FILE));
    framesFile.screenShots = 4;
    fs.writeFileSync(outputDir+"\\"+IMAGES_FOLDER+"\\"+IMAGE_INFO_FILE,JSON.stringify(framesFile));
    writePixelsToImage(outputDir+"\\"+IMAGES_FOLDER+"\\"+IMAGE_FILE.split(".")[0]+"_"+1+"."+IMAGE_FILE.split(".")[1],pixels1);
    writePixelsToImage(outputDir+"\\"+IMAGES_FOLDER+"\\"+IMAGE_FILE.split(".")[0]+"_"+2+"."+IMAGE_FILE.split(".")[1],pixels2);
    writePixelsToImage(outputDir+"\\"+IMAGES_FOLDER+"\\"+IMAGE_FILE.split(".")[0]+"_"+3+"."+IMAGE_FILE.split(".")[1],pixels3);
    writePixelsToImage(outputDir+"\\"+IMAGES_FOLDER+"\\"+IMAGE_FILE.split(".")[0]+"_"+4+"."+IMAGE_FILE.split(".")[1],pixels4);
    var matches = [];
    var i;
    for(i = 0; i<pixels1.length; i+=1){
        matches.push({queryIdx:i,trainIdx:i});
    }
    keypoints.writeKeypointsFile(outputDir+"\\"+KEYPOINTS_FOLDER+"\\"+KEYPOINT_FILE+0,matches,pixels1,pixels4);
    keypoints.writeKeypointsFile(outputDir+"\\"+KEYPOINTS_FOLDER+"\\"+KEYPOINT_FILE+1,matches,pixels2,pixels4);
    keypoints.writeKeypointsFile(outputDir+"\\"+KEYPOINTS_FOLDER+"\\"+KEYPOINT_FILE+2,matches,pixels3,pixels4);
    var keypointsFile = JSON.parse(fs.readFileSync(outputDir+"\\"+KEYPOINTS_FOLDER+"\\"+KEYPOINTS_INFO_FILE));
    keypointsFile.screenShots = framesFile.screenShots-1;
    fs.writeFileSync(outputDir+"\\"+KEYPOINTS_FOLDER+"\\"+KEYPOINTS_INFO_FILE,JSON.stringify(keypointsFile));  
    essentialMatrix.makeEssentialMatrices(outputDir+"\\",function(){
       writePureMotionMatrices(outputDir,0,0,1);
       model.makeModel(outputDir+"\\",function(){
           console.log("made synthetic model");
       }); 
    });
}

function experiment() {
    chainExperiment();
}

function writePureMotionMatrices(filepath,x,y,z){
    var essentialInfoFile = JSON.parse(fs.readFileSync(filepath+"\\"+ESSENTIAL_MATRICES_FOLDER+"\\"+ESSENTIAL_MATRIX_INFO_FILE));
    var i;
    for(i = 0; i<essentialInfoFile.essentialMatrices; i+=1){
        var curEssential =  JSON.parse(fs.readFileSync(filepath+"\\"+ESSENTIAL_MATRICES_FOLDER+"\\"+ESSENTIAL_MATRIX_FILE+i));
        curEssential.translation = [[x],[y],[z]];
        curEssential.rotation = [[1,0,0],[0,1,0],[0,0,1]];
        fs.writeFileSync(filepath+"\\"+ESSENTIAL_MATRICES_FOLDER+"\\"+ESSENTIAL_MATRIX_FILE+i,JSON.stringify(curEssential));
    }
}

module.exports.experiment = experiment;