'use strict';
var fs = require('fs-extra');
var ffmpeg = require('fluent-ffmpeg');

function makeImages(filePath, callback) {
    try {

        //Make sure that the google drive folder actually exists
        fs.statSync(filePath).isDirectory();
        try {

            //If the folder containing video frames already exists, wipe it
            fs.statSync(filePath + IMAGES_FOLDER).isDirectory();
            fs.removeSync(filePath + IMAGES_FOLDER);
        } catch (err) {}

        //Make the folder for holding the video frames and thumbnails
        fs.mkdirSync(filePath + IMAGES_FOLDER);
        fs.mkdirSync(filePath + IMAGES_FOLDER + '\\' + THUMBS_FOLDER);

        //Create the thumbnails using ffmpeg (also, get the length of the video)
        var numShots;
        var numSecs;
        //Create the actual screenshots(called async inside the thumbnails function)
        function makeScreenshots() {
            var timestamps = [];
            var timeIndex;
            var secondIndex;
            var curStamp = 0;
            //Fill the timestamps array with the video frame stamps we need to capture
            //TODO This is kind of clunky...
            for (secondIndex = 0; secondIndex <= numSecs; secondIndex += 1) {
                for (timeIndex = 0; timeIndex < SHOTS_PER_S; timeIndex += 1) {
                    if (timestamps.length < numShots) {
                        timestamps.push(secondIndex + "." + (Math.round(1000 / SHOTS_PER_S) * timeIndex));
                    }
                }
            }
            function writeSingleImg() {
                ffmpeg.ffprobe(filePath + VIDEO_FILE, function (err, metadata) {
                    ffmpeg(filePath + VIDEO_FILE).on('end', function () {
                        curStamp += 1;
                        //Write out the next timestamp image
                        if (curStamp < timestamps.length) {
                            writeSingleImg();
                        } else {
                            //Write out the # of video frames and the base image name and extension to the image info file
                            console.log('Made all the screenshots');
                            fs.writeFileSync(filePath + IMAGES_FOLDER + "\\" + IMAGE_INFO_FILE, JSON.stringify({
                                screenShots: numShots,
                                imageNames: IMAGE_FILE.split('.')[0],
                                imageExt: "." + IMAGE_FILE.split('.')[1]
                            }));
                            callback();
                        }
                    })
                        .on('error', function (err, stdout, stderr) {
                            console.log('ffmpeg had an error');
                            console.log(err.message);
                        })
                        .screenshots({
                            timestamps: [timestamps[curStamp]],
                            filename: IMAGE_FILE.split('.')[0] + '_' + (curStamp + 1) + '.png',
                            folder: filePath + IMAGES_FOLDER
                        });
                });
            }
            /**
            This recursively calls itself until it has moved through all of 
            the timestamps in the folder. I did it this way because ffmpeg
            would hold all of the images in memory and balloon up to 2gb
            or more if I tried to do all of the screenshots at once.
            **/
            writeSingleImg();
        }
        ffmpeg.ffprobe(filePath + VIDEO_FILE, function (err, metadata) {
            numSecs = Math.floor(metadata.format.duration);

            //TODO Jenky way of dealing with ffmpeg being slightly high on video length
            numShots = numSecs * SHOTS_PER_S - 1;
            if (SCREENSHOT_LIMIT && numShots > SCREENSHOT_LIMIT) {
                numShots = SCREENSHOT_LIMIT;
            }
            ffmpeg(filePath + VIDEO_FILE).on('end', function () {
                console.log('Made all the thumbnails');
                makeScreenshots();
            })
                .on('error', function (err, stdout, stderr) {
                    console.log(err.message);
                })
                .screenshots({
                    count: numShots,
                    filename: IMAGE_FILE,
                    folder: filePath + IMAGES_FOLDER + '\\' + THUMBS_FOLDER,
                    size: '90x162'
                });
        });
    } catch (err) {
        //We don't have that google drive folder...
    }
}

module.exports.makeImages = makeImages;