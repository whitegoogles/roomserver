'use strict';

var google = require('googleapis');
var GoogleAuth = require('google-auth-library');
var readline = require('readline');
var fs = require('fs-extra');
var request = require('request');

var authClient;
var isAuthorized = false;
var isOnline = false;

//Composed of a folder id, folder name, video file id, acc file id, gyro file id
//and a video file name, acc file name.
var folders = [];
var foldersToRead = [];
var filesToRead = [];
var AUTH_ERROR = 'Not authenticated';
var QUERY_ERROR = "Couldn't execute the query";
var DIR_ERROR = "Directory didn't have the right files";
var SUCCESS = 'Successfully read';

//I want to check online status, but I can't be forcing the client to wait
//for that response async
function checkOnline() {
	request('http://www.google.com',
		function (error, response) {
			isOnline = !error;
		}
        );
}

//If there isn't a token, use the mark tool in command prompt 
//to copy and paste the url. Enter it in, select the RoomModeler account, and copy//
//the code portion after the Cannot get ... code = part back into cmd prompt.
function authorize(credentials) {
    var auth = new GoogleAuth();
    authClient = new auth.OAuth2(credentials.web.client_id,
        credentials.web.client_secret,
        credentials.web.redirect_uris[0]);
    fs.readFile("token", function (err, token) {
        if (err) {
            //Lets get a token...
            var authUrl = authClient.generateAuthUrl({
                access_type: 'offline', //No idea what this is?
                scope: ['https://www.googleapis.com/auth/drive']
            });
            console.log('To authorize, go to this url: \n' + authUrl); //That offline thing makes more sense now I guess
            var promptAuth = readline.createInterface({
                input: process.stdin,
                output: process.stdout
            }); //I think this is piping cmd prompt input directly to this.
            promptAuth.question('Enter the code from that authorization window: ', function (code) {
                promptAuth.close(); //No more need to pipe in input
                authClient.getToken(code, function (err, token) {
                    console.log(err);
                    if (err) {
                        console.log('Something is wrong with getting the token');
                        isAuthorized = false;
                        return;
                    }
                    authClient.credentials = token;
                    fs.writeFile('token', JSON.stringify(token));
                    isAuthorized = true;
                });
            });
        } else {
            authClient.credentials = JSON.parse(token);
            isAuthorized = true;
        }
    });
}

function readCachedDirectory(folderName) {
    var titleFilePath = CACHE_FOLDER + "/" + folderName + '/' + TITLE_FILE;
    var folderObj = JSON.parse(fs.readFileSync(titleFilePath));
    if (folderObj && folderObj.folderId && folderObj.folderName && folderObj.videoId && folderObj.accId && folderObj.gyroId && folderObj.vidUrl && folderObj.accLink && folderObj.gyroLink) {
        return folderObj;
    }
    return null;
}

function readCachedFile(filePath) {
    try {
        if (fs.statSync(filePath).isFile()) {
            return filePath;
        }
    } catch (err) {
        return null;
    }
}

function queueFile(dirObj, fileId, fileUrl) {
    var index;
    var found;
    for (index = 0; index < filesToRead.length; index += 1) {
        if (filesToRead[index].fileId === fileId) {
            found = true;
            break;
        }
    }
    //Make sure they aren't already cached (pretty sure this is redundant but w/e)
    for (index = 0; index < folders.length; index += 1) {
        if ((folders[index].videoId === fileId && folders[index].videoFile) ||
                (folders[index].accId === fileId && folders[index].accFile) ||
                (folders[index].gyroId === fileId && folders[index].gyroFile)) {
            found = true;
            break;
        }
    }
    if (!found) {
        filesToRead.push({
            dirObj: dirObj,
            fileId: fileId,
            fileUrl: fileUrl,
            status: 'ready'
        });
    }
}

function queueFolder(folderId, folderTitle) {
    var index;
    var inQueue;
    for (index = 0; index < foldersToRead.length; index += 1) {
        if (foldersToRead[index].folderId === folderId) {
            inQueue = true;
            break;
        }
    }
    //Make sure they aren't already cached (could make a separate method)
    var cached;
    for (index = 0; index < folders.length; index += 1) {
        if (folders[index].folderId === folderId) {
            cached = true;
        }
    }
    if ((!cached) && (!inQueue)) {
        foldersToRead.push({
            folderTitle: folderTitle,
            folderId: folderId,
            status: 'ready'
        });
    }
}

function readCachedDirectories() { //Check for what we have in the cache
    folders = [];
    var folderNames = fs.readdirSync(CACHE_FOLDER); //Pull all the folders in the cache
    var i;
    console.log(folderNames);
    for (i = 0; i < folderNames.length; i += 1) { //Iterate through all directories in the cache folder
        try {
            var titleFilePath = CACHE_FOLDER + "/" + folderNames[i] + '/' + TITLE_FILE;
            if (fs.statSync(titleFilePath).isFile()) { //Does the current directory have a title file?
                var dirObj = readCachedDirectory(folderNames[i]); //Does the current directory have a title file?
                if (dirObj) {
                    var vidPath = CACHE_FOLDER + "/" + folderNames[i] + '/' + VIDEO_FILE;
                    var accPath = CACHE_FOLDER + "/" + folderNames[i] + '/' + ACC_FILE;
                    var gyroPath = CACHE_FOLDER + "/" + folderNames[i] + '/' + GYRO_FILE;
                    dirObj.videoFile = readCachedFile(vidPath);
                    dirObj.accFile = readCachedFile(accPath);
                    dirObj.gyroFile = readCachedFile(gyroPath);
                    if (!dirObj.videoFile) {
                        queueFile(dirObj, dirObj.videoId, dirObj.vidUrl); //Queue files that we are missing
                    }
                    if (!dirObj.accFile) {
                        queueFile(dirObj, dirObj.accId, dirObj.accLink); //Queue files that we are missing
                    }
                    if (!dirObj.gyroFile) {
                        queueFile(dirObj, dirObj.gyroId, dirObj.gyroLink); //Queue files that we are missing
                    }
                    folders.push(dirObj);
                }
            } //Think this is redundant with the try catch, so forget the else
        } catch (err) {
            console.log(err);
            //No luck, its not our directory because it has no title file
        }
    }
}

function readFolders() {
    if (isAuthorized && isOnline) {
        var service = google.drive('v2');
        service.files.list({
            auth: authClient,
            maxResults: 1000,
            q: "mimeType='application/vnd.google-apps.folder' and" +
                "trashed = false"
        }, function (err, response) {
            if (err) {
                console.log("Couldn't read any files from the google drive");
                console.log(err);
            } else {
                //console.log('Read the folders successfully');
                try {
                    var files = response.items;
                    var i;
                    for (i = 0; i < files.length; i += 1) {
                        //Queue them if we can't find them
                        queueFolder(files[i].id, files[i].title);
                    }
                } catch (err) {
                }
            }
        });
    }
}

function readDirectory(folderObj, callback) {
    var readFolder = folderObj.folderId;
    console.log(readFolder);
    if (isAuthorized && isOnline) {
        var service = google.drive('v2');
        service.files.list({
            auth: authClient,
            maxResults: 1000,
            q: "trashed = false and '" +
                readFolder + "' in parents"
        }, function (err, response) {
            if (err) {
                console.log("Couldn't read any files from that folder");
                console.log(err);
                callback(folderObj, QUERY_ERROR);
            } else {
                try {
                    var files = response.items;
                    var videoFile, accFile, gyroFile;
                    var i;
                    for (i = 0; i < files.length; i += 1) {
                        //console.log(files[i].mimeType);
                        console.log(files[i].title);
                        //console.log(files[i].exportLinks);
                        if (files[i].mimeType === 'video/mp4' && files[i].title === VIDEO_FILE) {
                            videoFile = files[i];
                        } else if (files[i].mimeType === 'application/vnd.google-apps.spreadsheet' && files[i].title === ACC_FILE) {
                            accFile = files[i];
                        } else if (files[i].mimeType === 'application/vnd.google-apps.spreadsheet' && files[i].title === GYRO_FILE) {
                            gyroFile = files[i];
                        }
                    }
                    if (videoFile && accFile && gyroFile) {
                        callback(folderObj, SUCCESS, videoFile.id, accFile.id, gyroFile.id,
                            videoFile.downloadUrl, accFile.exportLinks['text/csv'], gyroFile.exportLinks['text/csv']);
                    } else {
                        callback(folderObj, DIR_ERROR);
                    }
                } catch (err) {
                    callback(folderObj, QUERY_ERROR);
                }
            }
        });
    } else {
        callback(folderObj, AUTH_ERROR);
    }
}

function readFile(fileObj, callback) {
    request({
        url: fileObj.fileUrl,
        headers: {
            'Authorization': 'Bearer ' + authClient.credentials.access_token
        },
        encoding: null
    },
        function (error, response, body) {
            if (error) {
                callback(fileObj, QUERY_ERROR);
            } else {
                callback(fileObj, SUCCESS, body);
            }
        });
}

function updateDirectories() {
    var i;
    for (i = 0; i < foldersToRead.length; i += 1) {
        if (foldersToRead[i].status === 'ready') {
            foldersToRead[i].status = 'reading';
            readDirectory(foldersToRead[i], function (folderObj, status, vidId, accId, gyroId, vidUrl, accLink, gyroLink) {
                if (status === SUCCESS) {
                    //Cache the directory
                    var dirToMake = CACHE_FOLDER + '/' + folderObj.folderId;
                    try {
                        fs.statSync(dirToMake);
                    } catch (err) {
                        fs.mkdirSync(dirToMake);
                    }
                    var dirObj = {};
                    //Composed of a folder id, folder name, video file id, acc file id, gyro file id
                    //and a video file name, acc file name.
                    dirObj.folderId = folderObj.folderId;
                    dirObj.folderName = folderObj.folderTitle;
                    dirObj.videoId = vidId;
                    dirObj.accId = accId;
                    dirObj.gyroId = gyroId;
                    dirObj.vidUrl = vidUrl;
                    dirObj.accLink = accLink;
                    dirObj.gyroLink = gyroLink;
                    //Add it to the live list of directories
                    folders.push(dirObj);
                    //Cache the title file for later use
                    fs.writeFileSync(dirToMake + '/' + TITLE_FILE, JSON.stringify(dirObj));
                    //Queue the files being read
                    queueFile(dirObj, dirObj.videoId, vidUrl);
                    queueFile(dirObj, dirObj.accId, accLink);
                    queueFile(dirObj, dirObj.gyroId, gyroLink);
                    folderObj.status = 'read';
                } else if (status !== DIR_ERROR) {
                    folderObj.status = 'ready'; //Try again bud
                } else {

                    //If its a dir error, then it actually has the wrong kind of files in it, so forget it
                    folderObj.status = 'error';
                }
            });
        }
    }
}

function updateFiles() {
    var i;
    for (i = 0; i < filesToRead.length; i += 1) {
        if (filesToRead[i].status === 'ready') {
            filesToRead[i].status = 'reading';
            readFile(filesToRead[i], function (fileObj, status, body) {
                if (status === SUCCESS) {
                    var filePath = CACHE_FOLDER + '/' + fileObj.dirObj.folderId + '/';
                    if (fileObj.fileId === fileObj.dirObj.videoId) {
                        filePath += VIDEO_FILE;
                        fileObj.dirObj.videoFile = filePath;
                    } else if (fileObj.fileId === fileObj.dirObj.accId) {
                        filePath += ACC_FILE;
                        fileObj.dirObj.accFile = filePath;
                    } else {
                        filePath += GYRO_FILE;
                        fileObj.dirObj.gyroFile = filePath;
                    }
                    fs.writeFileSync(filePath, body);
                    fileObj.status = 'read';
                } else {
                    fileObj.status = 'ready';
                }
            });
        }
    }
}

function readSecretFile() {
    try {
        var content = fs.readFileSync(SECRET_FILE);
        authorize(JSON.parse(content));
    } catch (err) {
        console.log(err.message);
        console.log("Couldn't read the client secrets file");
        console.log(
            'Make sure to place the client_secrets file pasted \
from the google developer console into here.'
        );
        throw new Error('Secrets file not found'); //Kill the whole program. We really need that file
    }
}

function findFolderId(folderId) {
    var found;
    var index;
    for (index = 0; index < folders.length; index += 1) {
        if (folders[index].folderId === folderId && folders[index].videoFile && folders[index].accFile && folders[index].gyroFile) {
            found = true;
            break;
        }
    }
    return found;
}

module.exports.findFolderId = findFolderId;

module.exports.isOnline = function () {
    return isOnline;
};
module.exports.isAuthorized = function () {
    return isAuthorized;
};

module.exports.getFolders = function () {
    return folders;
};

checkOnline();
setInterval(checkOnline, 5000);
readSecretFile();
readCachedDirectories();
readFolders();
setInterval(readFolders, 5000); //Think I fixed this, shouldn't stack too many ajax calls
setInterval(updateDirectories, 5000);
setInterval(updateFiles, 5000);
