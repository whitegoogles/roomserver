'use strict';

//Globals to be shared with google drive.
global.SECRET_FILE = 'client_secret.json';
global.CACHE_FOLDER = 'www/cache';
global.TITLE_FILE = 'title';
global.VIDEO_FILE = 'Room.mp4';
global.ACC_FILE = 'Accelerometer.csv';
global.GYRO_FILE = 'Gyro.csv';
global.IMAGES_FOLDER = 'Images';
global.IMAGE_INFO_FILE = 'Image';
global.IMAGE_FILE = 'Frame.png';
global.THUMBS_FOLDER = 'thumb';
global.KEYPOINTS_FOLDER = 'Keypoints';
global.KEYPOINTS_INFO_FILE = 'Keypoint';
global.MATCH_IMAGE_FILE = 'MatchPic';
global.KEYPOINT_FILE = 'Matches';
global.ESSENTIAL_MATRICES_FOLDER = 'Essential Matrices';
global.ESSENTIAL_MATRIX_FILE = 'EssentialMatrix';
global.ESSENTIAL_MATRIX_INFO_FILE = 'EssentialMatrixInfo';
global.MODEL_FOLDER = 'Model';
global.MODEL_FILE = 'Model';
global.MODEL_PLY_FILE = 'Model.ply';
global.KEYPOINTS_CSV_FILE = 'KeypointCsv';
//How big of a region around the matched pixels should be incorporated in the point cloud?
global.REGION_SIZE = 30;//1;

//How many shots to take per second in each video? 
global.SHOTS_PER_S = 4;

//???????????? 
global.SCREENSHOT_LIMIT = 0;

//The image in the video sequence to compare to all of the other images
global.SIFT_LAST_IMG = 5; //20

//The number of image matches to incorporate in the final model.
global.MAX_MODEL_ITERATIONS = 3;

//The number of allowed SIFT matches(before RANSAC)
global.MAX_MATCHES = 200; //400

//Camera parameters
global.FOCAL_LENGTH = 1564;
global.CAM_CENTER_X = 962;
global.CAM_CENTER_Y = 565;

//libraries
var express = require('express');
var fs = require('fs-extra');
var request = require('request');
var cv = require('opencv');
var app = express();
var googleDrive = require('./googleDrive');
var frames = require('./frames');
var keypoints = require('./keypoints');
var model = require('./model');
var essentialMatrices = require('./essentialMatrices');
var experiments = require('./experiments');

app.set('view engine', 'jade');
app.set('views', 'www/html');
app.use(express.static('www'));

app.get('/checkStatus', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({
        isOnline: googleDrive.isOnline(),
        isAuthed: googleDrive.isAuthorized()
    }));
});
app.get('/readFolders', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({
        folders: googleDrive.getFolders()
    }));
});

function hasScreenshotsInfo(filePath) {
    try {
        var imagesInfo = JSON.parse(fs.readFileSync(filePath + IMAGES_FOLDER + "\\" + IMAGE_INFO_FILE));
        if (imagesInfo.screenShots) {
            return true;
        }
    } catch (err) {}
    return false;
}

function hasSIFTMatchesInfo(filePath) {
    try {
        var keypointsInfo = JSON.parse(fs.readFileSync(filePath + KEYPOINTS_FOLDER + "\\" + KEYPOINTS_INFO_FILE));
        if (keypointsInfo.screenShots) {
            return true;
        }
    } catch (err) {}
    return false;
}

function hasEssentialMatricesInfo(filePath) {
    try {
        var essentialMatricesInfo = JSON.parse(fs.readFileSync(filePath + ESSENTIAL_MATRICES_FOLDER + "\\" + ESSENTIAL_MATRIX_INFO_FILE));
        if (essentialMatricesInfo.essentialMatrices) {
            return true;
        }
    } catch (err) {}
    return false;
}

function hasModelInfo(filePath) {
    try {
        var modelInfo = JSON.parse(fs.readFileSync(filePath + MODEL_FOLDER + "\\" + MODEL_FILE));
        if (modelInfo.worldPts.length) {
            return true;
        }
    } catch (err) {}
    return false;
}


app.get('/convertVideo', function (req, res) {
    var filePath = req.query.curFolder.videoFile;
    var useCached = req.query.cached;
    filePath = filePath.replace(VIDEO_FILE, '');
    console.log('Converting ' + filePath);
    function convertFromEssentialMatrices() {
        model.makeModel(filePath, function () {
            console.log('Finished making the model');
        });
    }
    function convertFromSIFTMatches() {
        essentialMatrices.makeEssentialMatrices(filePath, function () {
            convertFromEssentialMatrices();
        });
    }
    function convertFromScreenshots() {
        keypoints.makeSiftMatches(filePath, function () {
            convertFromSIFTMatches();
        });
    }
    function convertFromScratch() {
        frames.makeImages(filePath, function () {
            convertFromScreenshots();
        });
    }
    if (useCached) {
        if (useCached.model) {
            if (hasModelInfo(filePath)) {
                console.log("display the model");
            } else {
                convertFromScratch();
            }
        } else if (useCached.matrices) {
            if (hasEssentialMatricesInfo(filePath)) {
                convertFromEssentialMatrices();
            } else {
                convertFromScratch();
            }
        } else if (useCached.siftMatches) {
            if (hasSIFTMatchesInfo(filePath)) {
                convertFromSIFTMatches();
            } else {
                convertFromScratch();
            }
        } else if (useCached.videoFrames) {
            if (hasScreenshotsInfo(filePath)) {
                convertFromScreenshots();
            } else {
                convertFromScratch();
            }
        } else {
            convertFromScratch();
        }
    } else {
        convertFromScratch();
    }
    res.end();
});

app.get('/', function (req, res) {
    res.render('SplashScreen');
});
app.get('/DataScreen', function (req, res) {
    var found;
    if (req.query.curFolder) {
        var folderId = req.query.curFolder.folderId;
        found = googleDrive.findFolderId(folderId);
    }
    if (found) {
        var videoUrl = req.query.curFolder.videoFile;
        videoUrl = videoUrl.replace(CACHE_FOLDER, '/cache'); //TODO This could be very unsafe?
        var folderPath = req.query.curFolder.videoFile.replace(VIDEO_FILE, '');
        console.log(folderPath);
        res.render('DataScreen', {
            curFolder: req.query.curFolder,
            videoUrl: videoUrl,
            hasCachedScreenshots: hasScreenshotsInfo(folderPath),
            hasCachedKeypoints: hasSIFTMatchesInfo(folderPath),
            hasCachedEssentialMatrices: hasEssentialMatricesInfo(folderPath),
            hasCachedModel: hasModelInfo(folderPath)
        });
    } else {
        res.render('SplashScreen');
    }
});

app.get(['/ProcessScreen', '/ProcessScreen/Originals'], function (req, res) {
    var found;
    if (req.query.curFolder) {
        var folderId = req.query.curFolder.folderId;
        found = googleDrive.findFolderId(folderId);
    }
    if (found) {
        var videoUrl = req.query.curFolder.videoFile;
        videoUrl = videoUrl.replace(CACHE_FOLDER, '/cache'); //TODO This could be very unsafe?
        res.render('ProcessScreen/Originals.jade', {
            curFolder: req.query.curFolder,
            videoUrl: videoUrl
        });
    } else {
        res.render('SplashScreen');
    }
});
app.get('/ProcessScreen/Images', function (req, res) {
    var found;
    if (req.query.curFolder) {
        var folderId = req.query.curFolder.folderId;
        found = googleDrive.findFolderId(folderId);
    }
    if (found) {
        var videoUrl = req.query.curFolder.videoFile;
        videoUrl = videoUrl.replace(CACHE_FOLDER, '/cache'); //TODO This could be very unsafe?
        res.render('ProcessScreen/Images.jade', {
            curFolder: req.query.curFolder,
            videoUrl: videoUrl
        });
    } else {
        res.render('SplashScreen');
    }
});
app.get('/ProcessScreen/SiftKeypoints', function (req, res) {
    var found;
    if (req.query.curFolder) {
        var folderId = req.query.curFolder.folderId;
        found = googleDrive.findFolderId(folderId);
    }
    if (found) {
        var videoUrl = req.query.curFolder.videoFile;
        videoUrl = videoUrl.replace(CACHE_FOLDER, '/cache'); //TODO This could be very unsafe?
        res.render('ProcessScreen/Keypoints.jade', {
            curFolder: req.query.curFolder,
            videoUrl: videoUrl
        });
    } else {
        res.render('SplashScreen');
    }
});

app.get('/ProcessScreen/Model', function (req, res) {
    var found;
    if (req.query.curFolder) {
        var folderId = req.query.curFolder.folderId;
        found = googleDrive.findFolderId(folderId);
    }
    if (found) {
        var videoUrl = req.query.curFolder.videoFile;
        videoUrl = videoUrl.replace(CACHE_FOLDER, '/cache'); //TODO This could be very unsafe?
        res.render('ProcessScreen/Model.jade', {
            curFolder: req.query.curFolder,
            videoUrl: videoUrl
        });
    } else {
        res.render('SplashScreen');
    }
});

app.get('/Status', function (req, res) {
    res.render('Status', {});
});
var server = app.listen(80, function () {
    console.log('Server listening on port 80');
});

experiments.experiment();