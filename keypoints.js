'use strict';
var cv = require('opencv');
var fs = require('fs-extra');
var essentialMatrix = require('./essentialMatrices');
var Canvas = require('canvas');

function removeDuplicatePoints(keypoints1) {
    var checkPoints = [];
    var a, b;
    for (a = 0; a < keypoints1.length; a += 1) {
        var found = false;
        for (b = 0; b < checkPoints.length; b += 1) {
            found = Math.abs(checkPoints[b].x - keypoints1[a].x) < 1 && Math.abs(checkPoints[b].y - keypoints1[a].y) < 1;
            if (found) {
                break;
            }
        }
        if (!found) {
            checkPoints.push(keypoints1[a]);
        }
    }
    return checkPoints;
}

function removeMatchClusters(matches, maxCount) {
    var queryMatchCount = [];
    var trainMatchCount = [];
    var i, j;
    for (i = 0; i < matches.length; i += 1) {
        var found = false;
        for (j = 0; j < queryMatchCount.length; j += 1) {
            if (queryMatchCount[j].queryIdx === matches[i].queryIdx) {
                queryMatchCount[j].count += 1;
                found = true;
                break;
            }
        }
        if (!found) {
            queryMatchCount.push({
                queryIdx: matches[i].queryIdx,
                count: 1
            });
        }
        found = false;
        for (j = 0; j < trainMatchCount.length; j += 1) {
            if (trainMatchCount[j].trainIdx === matches[i].trainIdx) {
                trainMatchCount[j].count += 1;
                found = true;
                break;
            }
        }
        if (!found) {
            trainMatchCount.push({
                trainIdx: matches[i].trainIdx,
                count: 1
            });
        }
    }
    var filteredMatches = [];
    for (i = 0; i < matches.length; i += 1) {
        var passedQueryMatch = false;
        for (j = 0; j < queryMatchCount.length; j += 1) {
            if (queryMatchCount[j].queryIdx === matches[i].queryIdx) {
                if (queryMatchCount[j].count <= maxCount) {
                    passedQueryMatch = true;
                }
                break;
            }
        }
        if (passedQueryMatch) {
            for (j = 0; j < trainMatchCount.length; j += 1) {
                if (trainMatchCount[j].trainIdx === matches[i].trainIdx) {
                    if (trainMatchCount[j].count <= maxCount) {
                        filteredMatches.push(matches[i]);
                    }
                    break;
                }
            }
        }
    }
    console.log("filtered matches " + filteredMatches.length);
    return filteredMatches;
}

function checkEpiConstraint(firstImgPts, secondImgPts, essentialMat) {
    var i;
    for (i = 0; i < firstImgPts.length; i += 1) {
        var firstImgPtMat = new cv.Matrix.Eye(1, 3);
        var secondImgPtMat = new cv.Matrix.Eye(3, 1);
        firstImgPtMat.set(0, 0, (firstImgPts[i].x - CAM_CENTER_X) / FOCAL_LENGTH);
        firstImgPtMat.set(0, 1, (firstImgPts[i].y - CAM_CENTER_Y) / FOCAL_LENGTH);
        firstImgPtMat.set(0, 2, 1);
        secondImgPtMat.set(0, 0, (secondImgPts[i].x - CAM_CENTER_X) / FOCAL_LENGTH);
        secondImgPtMat.set(1, 0, (secondImgPts[i].y - CAM_CENTER_Y) / FOCAL_LENGTH);
        secondImgPtMat.set(2, 0, 1);
        var tempMat = (firstImgPtMat.multiply(firstImgPtMat, essentialMat));
        var epiConstraint = tempMat.multiply(tempMat, essentialMat).get(0, 0);
        console.log("(" + firstImgPts[i].x + "," + firstImgPts[i].y + ") -> (" + secondImgPts[i].x + "," + secondImgPts[i].y + ") = " + epiConstraint);
    }
}

function getKeypoints(curMatches) {
    var curMatch;
    var firstImgPts = [];
    var secondImgPts = [];
    for (curMatch = 0; curMatch < curMatches.matches.length; curMatch += 1) {
        firstImgPts.push(curMatches.queryPoints[curMatches.matches[curMatch].queryIdx]);
        secondImgPts.push(curMatches.trainPoints[curMatches.matches[curMatch].trainIdx]);
    }
    return {
        firstImgPts: firstImgPts,
        secondImgPts: secondImgPts
    };
}

function makeSiftChain(filePath, callback) {
    clearDirectory(filePath);
    makeSiftPairs(filePath, function(scaleFiles){
        makeSiftPairs(filePath, function(chainFiles){
            console.log(chainFiles);
            console.log(scaleFiles);
            writeFiles(filePath,chainFiles,scaleFiles);
            callback();
        }, 0, 2);
    }, 0, 1);
}

function makeSiftMatches(filePath, callback) {
    clearDirectory(filePath);
    makeSiftPairs(filePath, function(files){
        writeFiles(filePath,files);
        callback();
    }, 0, 0, SIFT_LAST_IMG);
}

function clearDirectory(filePath){
    try {

        //Wipe the keypoints folder if it currently exists
        fs.statSync(filePath + KEYPOINTS_FOLDER).isDirectory();
        fs.removeSync(filePath + KEYPOINTS_FOLDER);
    } catch (err) {

    }

    //Make the keypoints folder and the thumbs folder
    fs.mkdirSync(filePath + KEYPOINTS_FOLDER);
    fs.mkdirSync(filePath + KEYPOINTS_FOLDER + "\\" + THUMBS_FOLDER);
}

//Maximg will make it compare to the last img instead of using second offset
function makeSiftPairs(filePath, callback, firstOffset, secondOffset, maxImg) {
    //Read the info file for the google drive folder to figure out what screenshots are available
    var imagesInfo = JSON.parse(fs.readFileSync(filePath + IMAGES_FOLDER + "\\" + IMAGE_INFO_FILE));
    var i, counter,files;
    counter = 0;
    files = [];
    /**
    Iterate through the screenshots, finding SIFT correspondences between the 
    1st one and each successive one.
    **/
    var maxShot = imagesInfo.screenShots;
    if (maxImg && maxImg < imagesInfo.screenShots) {
        maxShot = maxImg;
    }
    for (i = firstOffset + 1; i + secondOffset <= maxShot; i += 1) {
        var lastImgIdx = i + secondOffset;
        if (maxImg) {
            lastImgIdx = maxShot;
        }
        //Read the first image 
        cv.readImage(filePath + IMAGES_FOLDER + '/' + imagesInfo.imageNames + "_" + (i) + imagesInfo.imageExt, function (err, mat1) {
            cv.readImage(filePath + IMAGES_FOLDER + '/' + imagesInfo.imageNames + "_" + (lastImgIdx) + imagesInfo.imageExt, function (err, mat2) {
                var img1Src = fs.readFileSync(filePath + IMAGES_FOLDER + '/' + imagesInfo.imageNames + "_" + (i) + imagesInfo.imageExt);
                var img2Src = fs.readFileSync(filePath + IMAGES_FOLDER + '/' + imagesInfo.imageNames + "_" + (lastImgIdx) + imagesInfo.imageExt);
                var madePairs = matchSiftPair(mat1, mat2, img1Src, img2Src, filePath, "" + i + "-" + lastImgIdx);
                if (!madePairs) {
                    console.log("Couldn't find keypoints for " + i + "-" + lastImgIdx);
                }
                else{
                    files.push({
                        img1:i,
                        img2:lastImgIdx,
                        filename:"" + i + "-" + lastImgIdx
                    });
                }
                counter += 1;
                if (counter +secondOffset>=maxShot) {
                    callback(files);
                }
            });
        });
    }
}

function writeFiles(filePath,chainFiles,scaleFiles){
    console.log('Made all keypoint matches');
    var imagesInfo = JSON.parse(fs.readFileSync(filePath + IMAGES_FOLDER + "\\" + IMAGE_INFO_FILE));
    //We've converted the last one
    fs.writeFileSync(filePath + KEYPOINTS_FOLDER + "\\" + KEYPOINTS_INFO_FILE, JSON.stringify({
        matchNames: KEYPOINT_FILE,
        chainFiles:chainFiles,
        scaleFiles:scaleFiles,
        imageNames: imagesInfo.imageNames,
        imageExt: imagesInfo.imageExt
    }));
}

function matchSiftPair(mat1, mat2, img1Src, img2Src, filePath, fileExtension) {
    mat1.convertGrayscale();
    var keypoints1 = mat1.sift();
    keypoints1 = removeDuplicatePoints(keypoints1);
    var desc1 = mat1.extract();
    mat2.convertGrayscale();
    var keypoints2 = mat2.sift();
    keypoints2 = removeDuplicatePoints(keypoints2);
    var desc2 = mat2.extract();
    var matches;
    if (keypoints1.length === 0 || keypoints2.length === 0) {
        return false;
    } else {
        var oldMatches = desc1.match(desc2);
        matches = removeMatchClusters(oldMatches, 1);
    }
    var allImgPts = getKeypoints({
        matches: matches,
        queryPoints: keypoints1,
        trainPoints: keypoints2
    });
    var ransacMasks = essentialMatrix.findEssentialMatrix(allImgPts.firstImgPts, allImgPts.secondImgPts).essentialMask;
    var filteredMatchIdx;
    var ransacFilteredMatches = [];
    for (filteredMatchIdx = 0; filteredMatchIdx < ransacMasks.length; filteredMatchIdx += 1) {
        if (ransacMasks[filteredMatchIdx].notMasked) {
            ransacFilteredMatches.push(matches[filteredMatchIdx]);
        }
    }
    matches = ransacFilteredMatches;
    var img1 = new Canvas.Image();
    var img2 = new Canvas.Image();
    img1.src = img1Src;
    img2.src = img2Src;
    var cnv = new Canvas(img1.width + img2.width, img1.height);
    var ctx = cnv.getContext('2d');
    ctx.drawImage(img1, 0, 0);
    ctx.drawImage(img2, img1.width, 0);
    var matchIdx;
    var filteredMatches = [];
    var firstImgPts = [];
    var secondImgPts = [];
    var queryPt;
    var trainPt;
    for (matchIdx = 0; matchIdx < MAX_MATCHES && matches && matchIdx < matches.length; matchIdx += 1) {
        queryPt = keypoints1[matches[matchIdx].queryIdx];
        firstImgPts.push(queryPt);
        trainPt = keypoints2[matches[matchIdx].trainIdx];
        secondImgPts.push(trainPt);
        filteredMatches.push(matches[matchIdx]);
    }
    matches = filteredMatches;
    writeKeypointsFile(filePath + KEYPOINTS_FOLDER + "\\" + KEYPOINT_FILE + fileExtension, matches, keypoints1, keypoints2);
    //Write it to a csv file as well for matlab
    var j;
    var curMatchesInfo = JSON.parse(fs.readFileSync(filePath + KEYPOINTS_FOLDER + "\\" + KEYPOINT_FILE + fileExtension));
    var imgPts = getKeypoints(curMatchesInfo);
    var pointsFileStr = "";
    for (j = 0; j < imgPts.firstImgPts.length; j += 1) {
        pointsFileStr += imgPts.firstImgPts[j].x + "," + imgPts.firstImgPts[j].y + "," + imgPts.secondImgPts[j].x + "," + imgPts.secondImgPts[j].y + '\n';
    }
    fs.writeFileSync(filePath + KEYPOINTS_FOLDER + "\\" + KEYPOINTS_CSV_FILE + fileExtension + ".csv", pointsFileStr);

    var curIdx;

    //TODO Testing with keypoints instead of matches
    //firstImgPts = keypoints1;
    //secondImgPts = keypoints2;
    for (curIdx = 0; curIdx < secondImgPts.length && curIdx < firstImgPts.length; curIdx += 1) {
        var colorStr = "#" + (Math.floor(Math.random() * 255)).toString(16) + (Math.floor(Math.random() * 255)).toString(16) + (Math.floor(Math.random() * 255)).toString(16);
        queryPt = firstImgPts[curIdx];
        trainPt = secondImgPts[curIdx];
        ctx.strokeStyle = colorStr;
        ctx.lineWidth = 3;
        //Query points	
        ctx.beginPath();
        ctx.arc(queryPt.x, queryPt.y, 3, 0, 2 * Math.PI, false);
        ctx.stroke();
        //Train points
        ctx.beginPath();
        ctx.arc(trainPt.x + img1.width, trainPt.y, 3, 0, 2 * Math.PI, false);
        ctx.stroke();
        //ctx.strokeStyle = "#ffffff";
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.moveTo(trainPt.x + img1.width, trainPt.y);
        ctx.lineTo(queryPt.x, queryPt.y);
        ctx.stroke();
    }
    fs.writeFileSync(filePath + KEYPOINTS_FOLDER + "\\" + MATCH_IMAGE_FILE + fileExtension + ".png", cnv.toBuffer());
    var scale = 8;
    cnv = new Canvas((img1.width + img2.width) / scale, img1.height / scale);
    ctx = cnv.getContext('2d');
    ctx.scale(1 / scale, 1 / scale);
    var thumbImgSrc = fs.readFileSync(filePath + KEYPOINTS_FOLDER + "\\" + MATCH_IMAGE_FILE + fileExtension + ".png");
    var thumbImg = new Canvas.Image();
    thumbImg.src = thumbImgSrc;
    ctx.drawImage(thumbImg, 0, 0);
    fs.writeFileSync(filePath + KEYPOINTS_FOLDER + "\\" + THUMBS_FOLDER + "\\" + MATCH_IMAGE_FILE + fileExtension + ".png", cnv.toBuffer());
    mat2.release();
    mat1.release();
    return true;
}

function writeKeypointsFile(filePath, matches, queryPoints, trainPoints) {
    fs.writeFileSync(filePath,
        JSON.stringify({
            matches: matches,
            queryPoints: queryPoints,
            trainPoints: trainPoints
        }));
}

module.exports.writeKeypointsFile = writeKeypointsFile;
module.exports.getKeypoints = getKeypoints;
module.exports.makeSiftMatches = makeSiftMatches;
module.exports.makeSiftChain = makeSiftChain;