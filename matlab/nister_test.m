% Q1 = rand(3,50);
% Q1(3,:) = 1;
% Q2 = Q1;
% Q2(1,:) = Q2(1,:)+1;
% Q2 = Q2/2;
% Q1 = Q1/2;
ptsSize = size(firstImgPtsX);
numPts = ptsSize(1);
Q1 = ones(3,numPts);
Q2 = ones(3,numPts);
Q1(1,:) = firstImgPtsX;
Q1(2,:) = firstImgPtsY;
Q2(1,:) = secondImgPtsX;
Q2(2,:) = secondImgPtsY;
focalLength = 1564;
centerX = 962;
centerY = 565;
Q1(1,:) = (Q1(1,:)-centerX)/focalLength;
Q2(1,:) = (Q2(1,:)-centerX)/focalLength;
Q1(2,:) = (Q1(2,:)-centerX)/focalLength;
Q2(2,:) = (Q2(2,:)-centerX)/focalLength;

%Book recommends dividing by unit variance, so sure
%but it worked terribly so guess not
% Q1(1,:) = Q1(1,:)/std(Q1(1,:));
% Q1(2,:) = Q1(2,:)/std(Q1(2,:));
% Q2(1,:) = Q2(1,:)/std(Q2(1,:));
% Q2(2,:) = Q2(2,:)/std(Q2(2,:));

Evec = calibrated_fivepoint(Q1,Q2);
for i=1:size(Evec,2)
   E = reshape(Evec(:,i),3,3);
   % Check determinant constraint! 
%   det( E)
%   % Check trace constraint
%   2 *E*transpose(E)*E -trace( E*transpose(E))*E
%   % Check reprojection errors
   diag( Q1'*E*Q2);
   %get svd of essential mat
   [U,S,V] = svd(E);
   %print translation matrix
   U(:,3)
 end