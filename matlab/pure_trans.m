%Test Case
%Moving from 0,0 to 0,1
%X: [0,1] - > [0,1]
%Y: [0,1] - > [1,2]

%Moving from 0,0 to 1,0
%X: [0,1] - > [1,2]
%Y: [0,1] - > [0,1]
function [trans] = pure_trans(firstImgPtsX,firstImgPtsY,secondImgPtsX,secondImgPtsY,centerX,centerY,focalLength)
    ptsSize = size(firstImgPtsX);
    numPoints = ptsSize(1);
    firstImgPtsX = (firstImgPtsX - centerX)/focalLength;
    secondImgPtsX = (secondImgPtsX - centerX)/focalLength;
    firstImgPtsY = (firstImgPtsY - centerY)/focalLength;
    secondImgPtsY = (secondImgPtsY - centerY)/focalLength;
%     firstImgPtsX = firstImgPtsX/var(firstImgPtsX);
%     secondImgPtsX = secondImgPtsX/var(secondImgPtsX);
%     firstImgPtsY = firstImgPtsY/var(firstImgPtsY);
%     secondImgPtsY = secondImgPtsY/var(secondImgPtsY);
    eMat = ones(2,3);
    solsSum = zeros(3,1);
    for j = 1 : numPoints-2
        for i = 1 : 2
            eMat(i,1) = secondImgPtsY(i+j)*firstImgPtsX(i+j)-firstImgPtsY(i+j)*secondImgPtsX(i+j);
            eMat(i,2) = firstImgPtsX(i+j)-secondImgPtsX(i+j);
            eMat(i,3) = firstImgPtsY(i+j)-secondImgPtsY(i+j);
        end
        solsSum = solsSum +null(eMat);
    end
    sols = solsSum/(numPoints-1);
    E = [0 -sols(1) -sols(2);
         sols(1) 0 -sols(3);
         sols(2) sols(3) 0];
    %get svd of essential mat
    [U,S,V] = svd(E);
    %print translation matrix
    trans = U(:,3);
    %trans
end