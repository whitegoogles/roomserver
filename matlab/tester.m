function tester()
    baseFolder = '../www/cache/test-x/'
    jsFolderIn = strcat(baseFolder,'Keypoints/');
    jsFolderOut = strcat(baseFolder,'Essential Matrices/');
    numShots = 13;
    for i = 0: numShots
        [x1,y1,x2,y2] = readKeypoint(jsFolderIn,i);
        trans = pure_trans(x1,y1,x2,y2,962,565,1564)
        %[1 0 0; 0 1 0; 0 0 1]
        rot = [0.9999 -0.01 -0.0001;0.001 0.99999 -0.0003;0.00001 -0.0005 0.999999];
        writeEssentialMat(strcat(jsFolderOut,'EssentialMatrix',num2str(i)),trans,rot);
    end
end

function [x1,y1,x2,y2] = readKeypoint(folder,curShot)
    filename = strcat(folder,'KeypointCSV');
    filename = strcat(filename,num2str(curShot),'.csv')
    file = importdata(filename,',',0);
    x1 = file(:,1);
    y1 = file(:,2);
    x2 = file(:,3);
    y2 = file(:,4);
end

function writeEssentialMat(fileName,transMat,rotMat)
    file = fopen(fileName,'w');
    fprintf(file,'%s','{');
    fprintf(file,'%s','"translation":');
    printMatToFile(file,transMat);
    fprintf(file,'%s',',');
    fprintf(file,'%s','"rotation":');
    printMatToFile(file,rotMat);
    fprintf(file,'%s','}');
    fclose(file);
end

function printMatToFile(file,mat)
    transMatSize = size(mat);
    transRows = transMatSize(1);
    transCols = transMatSize(2);
    fprintf(file,'%s','[');
    for i = 1 : transRows
        fprintf(file,'%s','[');
        for j = 1 : transCols
            fprintf(file,'%.6f',mat(i,j));
            if(j<transCols)
                fprintf(file,'%s',',');
            end
        end
        fprintf(file,'%s',']');
        if(i<transRows)
            fprintf(file,'%s',',');
        end
    end
    fprintf(file,'%s',']');
end