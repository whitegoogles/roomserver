x1 = 616.85611225;
y1 = 403.526449319;

x2 = 831.4651716075;
y2 = 503.9300434508;

pt1 = [x1 y1 1 1];
pt2 = [x2 y2 1 1];

firstCam = [1      0      0.01    0.185;...
               -0.001 0.9999 -0.0004 -0.3295;...
               -0.012 0.0005 0.9999  -0.926;...
               0      0      0       1];
secondCam = [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1];
invCam = [0.000639 0 -0.615 0;
          0        0.000639 -0.361 0;
          0 0 1 0;
          0 0 0 1];

firstCamInv = inv(firstCam);

transMat = [1,0,0]';
firstWorldPt = [10,0,5]';%firstCamInv * invCam*pt1';

secondWorldPt = [-10,0,5]';%invCam*pt2';

firstVec = firstWorldPt;%firstCam(:,4);

secondVec = secondWorldPt - transMat;%secondCam(:,4);

%epipoleVec = firstCam(:,4)-secondCam(:,4);
epipoleVec = firstWorldPt - secondWorldPt;
firstVec = firstVec/norm(firstVec);
secondVec = secondVec/norm(secondVec);
epipoleVec = epipoleVec/norm(epipoleVec);

firstVec
secondVec
epipoleVec
a = dot(firstVec,firstVec);
b = dot(firstVec,secondVec);
c = dot(secondVec,secondVec);
d = dot(firstVec,epipoleVec);
e = dot(secondVec,epipoleVec);

para1 = (b*e-c*d)/(a*c-b*b);
para2 = (a*e-b*d)/(a*c-b*b);

world1 = firstWorldPt+para1*firstVec;
world2 = secondWorldPt+para2*secondVec;
world1
world2
(world1+world2)/2


