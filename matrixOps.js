'use strict';

module.exports.chopMatrix = function chopMatrix(mat) {
    mat.resize(3, 3);
};

module.exports.printMatrix = function printMatrix(mat, matName) {
    var matArray = [];
    if (mat.size && mat.get) {
        var rows = (mat.size())[0];
        var cols = (mat.size())[1];
        var rowStr = "[";
        var curRow, curCol;
        console.log(matName + ":");
        for (curRow = 0; curRow < rows; curRow += 1) {
            matArray.push([]);
            for (curCol = 0; curCol < cols; curCol += 1) {
                matArray[curRow][curCol] = mat.get(curRow, curCol);
                rowStr += mat.get(curRow, curCol) + " ";
            }
            console.log(rowStr + "]");
            rowStr = "[ ";
        }
    } else {
        console.log(JSON.stringify(mat) + " is not a matrix...");
    }
    return matArray;
};

module.exports.normalize = function normalize(mat) {
    var i;
    var numRows = (mat.size())[0];
    var curSquare = 0;
    for (i = 0; i < numRows; i += 1) {
        curSquare += mat.get(i, 0) * mat.get(i, 0);
    }
    curSquare = Math.sqrt(curSquare);
    for (i = 0; i < numRows; i += 1) {
        mat.set(i, 0, mat.get(i, 0) / curSquare);
    }
    return mat;
};

module.exports.getDist = function getDist(p) {
    return Math.sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
};