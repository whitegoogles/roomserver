'use strict';

var fs = require('fs-extra');
var keypoints = require('./keypoints');
var cv = require('opencv');
var matrixOps = require('./matrixOps');

function getWorldPoint(pixelPt, cameraMat, invCameraMatrix) {
    var pixelMat = new cv.Matrix.Eye(4, 1);
    pixelMat.set(0, 0, pixelPt.x);
    pixelMat.set(1, 0, pixelPt.y);
    pixelMat.set(2, 0, 1);
    pixelMat.set(3, 0, 1);
    
    //x  = K[R | t]X, so [R|t]'K'x = X (' is inverse)
    var projectedPointMat = (invCameraMatrix.multiply(invCameraMatrix, pixelMat));
    var worldPointMat = cameraMat.multiply(cameraMat, projectedPointMat); //projectedPointMat
    var cutPointMat = new cv.Matrix.Eye(3, 1);
    cutPointMat.set(0, 0, worldPointMat.get(0, 0));
    cutPointMat.set(1, 0, worldPointMat.get(1, 0));
    cutPointMat.set(2, 0, worldPointMat.get(2, 0));
    
    //Free up memory    
    pixelMat.release();
    projectedPointMat.release();
    worldPointMat.release();
    return cutPointMat;
}

function triangulateLinear(firstPixelPt, secondPixelPt, firstCameraMat, secondCameraMat, invCameraMatrix) {
    
    //Fix the pixel point with the x=PX equation by using the inverse of to find x
    //This gives us the actual 3d world point (without scale) from the pixel
    //using the camera and rotation/translation matrices
    var firstWorldMat = getWorldPoint(firstPixelPt, firstCameraMat, invCameraMatrix);
    var secondWorldMat = getWorldPoint(secondPixelPt, secondCameraMat, invCameraMatrix);
    function getTransMat(camMat) {
        var transMat = new cv.Matrix.Eye(3, 1);
        transMat.set(0, 0, camMat.get(0, 3));
        transMat.set(1, 0, camMat.get(1, 3));
        transMat.set(2, 0, camMat.get(2, 3));
        return transMat;
    }
    function getRotMat(camMat){
        var rotMat = new cv.Matrix.Eye(3,3);
        var i,j;
        for(i=0; i<3; i+=1){
            for(j = 0; j<3; j+=1){
                rotMat.set(i,j,camMat.get(i,j));
            }
        }
        return rotMat;
    }    
    
    //TODO Get a test case working with synthetic data for this
    firstCameraMat = getTransMat(firstCameraMat);
    secondCameraMat = getTransMat(secondCameraMat);
    var firstCameraRotMat = getRotMat(firstCameraMat);
    var secondCameraRotMat = getRotMat(secondCameraMat);
    //Compute the vectors lying along the lines defined by the camera centers
    //and the projected world points (firstWorldMat and secondWorldMat)
    var firstWorldVector = firstWorldMat.sub(firstWorldMat, firstCameraMat);
    var secondWorldVector = secondWorldMat.sub(secondWorldMat,secondCameraMat);
    var epipoleVector = firstCameraMat.sub(firstCameraMat, secondCameraMat);
    
    //Compute some important dot products for finding the distance (parametrically)
    //to move along each line through the project points and the camera points.
    var a = (firstWorldVector.dot(firstWorldVector, firstWorldVector));
    var b = (firstWorldVector.dot(firstWorldVector, secondWorldVector));
    var c = (firstWorldVector.dot(secondWorldVector, secondWorldVector));
    var d = (firstWorldVector.dot(firstWorldVector, epipoleVector));
    var e = (firstWorldVector.dot(secondWorldVector, epipoleVector));
    
    //Find the distance (parametrically)along each world line (as defined above)
    var firstLineDist = (b * e - c * d) / (a * c - b * b);
    var secondLineDist = (a * e - b * d) / (a * c - b * b);
    var firstLineDistMat = new cv.Matrix.Eye(3, 3);
    var secondLineDistMat = new cv.Matrix.Eye(3, 3);
    
    //Set up some matrices to act as scalar multipliers so we can move the correct
    //parametric amount along each respective world line
    firstLineDistMat.set(0, 0, firstLineDist);
    firstLineDistMat.set(1, 1, firstLineDist);
    firstLineDistMat.set(2, 2, firstLineDist);
    secondLineDistMat.set(0, 0, secondLineDist);
    secondLineDistMat.set(1, 1, secondLineDist);
    secondLineDistMat.set(2, 2, secondLineDist);
    var firstOptVector = firstLineDistMat.multiply(firstLineDistMat, firstWorldVector);
    var secondOptVector = secondLineDistMat.multiply(secondLineDistMat, secondWorldVector);
    var firstOptPoint = firstCameraMat.add(firstCameraMat, firstOptVector);
    var secondOptPoint = secondCameraMat.add(secondCameraMat, secondOptVector);
    var optPoint = {};
    optPoint.x = (firstOptPoint.get(0, 0) + secondOptPoint.get(0, 0)) / 2;
    optPoint.y = (firstOptPoint.get(0, 1) + secondOptPoint.get(0, 1)) / 2;
    optPoint.z = (firstOptPoint.get(0, 2) + secondOptPoint.get(0, 2)) / 2;
    var error = matrixOps.getDist({x:firstOptPoint.get(0,0)-secondOptPoint.get(0,0),y:firstOptPoint.get(0,1)-secondOptPoint.get(0,1),z:firstOptPoint.get(0,2)-secondOptPoint.get(0,2)});
    var optMat = new cv.Matrix.Eye(3,1);
    if (firstLineDist < 0 || secondLineDist < 0) {
        //console.log("Triangulated behind the camera");
    }
    
    //Free up memory even though we're in javascript uggg
    firstWorldMat.release();
    secondWorldMat.release();
    firstWorldVector.release();
    secondWorldVector.release();
    firstLineDistMat.release();
    secondLineDistMat.release();
    firstOptPoint.release();
    secondOptPoint.release();
    firstCameraMat.release();
    secondCameraMat.release();
    return optPoint;
}

/*function useDist(threeImgMatches, firstCameraMat, secondCameraMat, invCameraMatrix) {
    var distScaler = 1;
    var i;
    for (i = 0; i < threeImgMatches.length; i += 1) {
        var curImgMatch = threeImgMatches[i];
        var new3DPt = triangulateLinear(curImgMatch.firstImgPt, curImgMatch.secondImgPt, firstCameraMat, secondCameraMat, invCameraMatrix);
        var last3DPt = curImgMatch.last3DPt;
        if (new3DPt) {
            distScaler += matrixOps.getDist(last3DPt) / matrixOps.getDist(new3DPt);
        }
    }
}*/

function useScale(points) {

}

//http://potree.org/demo/plyViewer/plyViewer.html
/*
function triangulatePoints(invCameraMatrix, firstPixelPt, curCamPt) {
    //console.log(curCamPt);
    var firstPixelPtMat = new cv.Matrix.Eye(3, 1);
    firstPixelPtMat.set(0, 0, firstPixelPt.x);
    firstPixelPtMat.set(1, 0, firstPixelPt.y);
    firstPixelPtMat.set(2, 0, 1);
    var worldLine = invCameraMatrix.multiply(invCameraMatrix, firstPixelPtMat);
    //printMatrix(worldLine,'world line');
    var worldLineNormalizer = Math.sqrt(worldLine.get(0, 0) * worldLine.get(0, 0) + worldLine.get(1, 0) * worldLine.get(1, 0) + worldLine.get(2, 0) * worldLine.get(2, 0));
    worldLine.set(0, 0, worldLine.get(0, 0) / worldLineNormalizer);
    worldLine.set(1, 0, worldLine.get(1, 0) / worldLineNormalizer);
    worldLine.set(2, 0, worldLine.get(2, 0) / worldLineNormalizer);
    //printMatrix(worldLine,'normalized world line');
    var worldLineTrans = worldLine.transpose(worldLine);
    var worldLineScalar = worldLine.multiply(worldLine, worldLineTrans).get(0, 0);
    //printMatrix(worldLineScalar,'world line scalar');
    var negWorldLineEye = new cv.Matrix.Eye(3, 3);
    var curRow, curCol;
    for (curRow = 0; curRow < 3; curRow += 1) {
        for (curCol = 0; curCol < 3; curCol += 1) {
            negWorldLineEye.set(curRow, curCol, -worldLineScalar);
        }
    }
    negWorldLineEye.set(0, 0, 1 - worldLineScalar);
    negWorldLineEye.set(1, 1, 1 - worldLineScalar);
    negWorldLineEye.set(2, 2, 1 - worldLineScalar);
    //printMatrix(negWorldLineEye,'negWorldLineEye');
    var curCamPtMat = new cv.Matrix.Eye(3, 1);
    curCamPtMat.set(0, 0, curCamPt[0][0]);
    curCamPtMat.set(1, 0, curCamPt[1][0]);
    curCamPtMat.set(2, 0, curCamPt[2][0]);
    var worldLineCam = negWorldLineEye.multiply(negWorldLineEye, curCamPtMat);
    //printMatrix(worldLineCam,'worldLineCam');
    return {
        worldLineCam: worldLineCam,
        negWorldLineEye: negWorldLineEye
    };
}
*/
function getCameraMatrix() {
    var cameraMatrix = new cv.Matrix.Eye(4,4);
    var curRow,curCol;
    for (curRow = 0; curRow < 4; curRow += 1) {
        for (curCol = 0; curCol < 4; curCol += 1) {
            cameraMatrix.set(curRow, curCol, 0);
        }
    }
    cameraMatrix.set(0, 0, FOCAL_LENGTH);
    cameraMatrix.set(1, 1, FOCAL_LENGTH);
    cameraMatrix.set(0, 2, CAM_CENTER_X);
    cameraMatrix.set(1, 2, CAM_CENTER_Y);
    cameraMatrix.set(2, 2, 1);
    cameraMatrix.set(3, 3, 1);
    return cameraMatrix;
}

function makeModel(filePath, callback) {

    try {

        //Make sure the directory for the google folder even exists
        fs.statSync(filePath).isDirectory();
        try {

            //Wipe the old model folder
            fs.statSync(filePath + MODEL_FOLDER).isDirectory();
            fs.removeSync(filePath + MODEL_FOLDER);
        } catch (err) {

        }

        //Create the new model folder
        fs.mkdirSync(filePath + MODEL_FOLDER);
        fs.mkdirSync(filePath + MODEL_FOLDER+"\\Scales");
        var essentialMatricesInfo = JSON.parse(fs.readFileSync(filePath + ESSENTIAL_MATRICES_FOLDER + '\\' + ESSENTIAL_MATRIX_INFO_FILE));
        var keypointsInfo = JSON.parse(fs.readFileSync(filePath + KEYPOINTS_FOLDER + '\\' + KEYPOINTS_INFO_FILE));
        var screenshotsInfo = JSON.parse(fs.readFileSync(filePath + IMAGES_FOLDER + '\\' + IMAGE_INFO_FILE));
        var curEssentialMatrix;
        var cameraTranslations = [];
        var cameraRotations = [];
        var worldPts = [];
        cameraTranslations.push([
            [0],
            [0],
            [0]
        ]);
        cameraRotations.push([
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1]
        ]);
        for (curEssentialMatrix = 0; curEssentialMatrix < essentialMatricesInfo.essentialMatrices; curEssentialMatrix += 1) {
            var curEssentialMatrixInfo = JSON.parse(fs.readFileSync(filePath + ESSENTIAL_MATRICES_FOLDER + '\\' + essentialMatricesInfo.essentialMatrixNames + curEssentialMatrix));
            var curTranslation = curEssentialMatrixInfo.translation;
            var curRotation = curEssentialMatrixInfo.rotation;
                cameraTranslations.push(curTranslation);
                cameraRotations.push(curRotation);
        }
        var curKeypoints;
        var allImgPts;
        var lastImgPts;

        //Some of these can be undefined (check for this!)
        var last3DPts;
        var maxModelIters = keypointsInfo.screenShots;
        if (MAX_MODEL_ITERATIONS && MAX_MODEL_ITERATIONS < keypointsInfo.screenShots) {
            maxModelIters = MAX_MODEL_ITERATIONS;
        }
        for (curKeypoints = 0; curKeypoints < maxModelIters; curKeypoints += 1) {
            var curKeypointsInfo = JSON.parse(fs.readFileSync(filePath + KEYPOINTS_FOLDER + '\\' + keypointsInfo.matchNames + curKeypoints));
            allImgPts = keypoints.getKeypoints(curKeypointsInfo);
            var curImgPt;

            //TODO Refactor this into a constant
            if (allImgPts.firstImgPts.length >= 5 && allImgPts.secondImgPts.length >= 5) {
                var cameraMatrix = getCameraMatrix();
                //printMatrix(cameraMatrix,"Camera Matrix");
                var invCameraMatrix = cameraMatrix.inverse(cameraMatrix);
                //printMatrix(invCameraMatrix,"Inv. Camera Matrix");
                cv.readImage(filePath + IMAGES_FOLDER + '/' + screenshotsInfo.imageNames + '_' + (curKeypoints + 1) + screenshotsInfo.imageExt, function (err, img) {

                    function makeCompositeMat(transMat, rotMat) {
                        var camMat = new cv.Matrix.Eye(4, 4);
                        var i, j;
                        for (i = 0; i < 3; i += 1) {
                            for (j = 0; j < 3; j += 1) {
                                camMat.set(i, j, rotMat[i][j]);
                            }
                        }
                        camMat.set(0, 3, transMat[0][0]);
                        camMat.set(1, 3, transMat[1][0]);
                        camMat.set(2, 3, transMat[2][0]);
                        camMat.set(3, 3, 1);
                        return camMat;
                    }
                    var firstCameraMat = makeCompositeMat(cameraTranslations[curKeypoints + 1], cameraRotations[curKeypoints + 1]);
                    var secondCameraMat = makeCompositeMat(cameraTranslations[0], cameraRotations[0]);

                    //TODO This is a slow way of doing this....
                    //Find matches between the the img 2 points that match img 1 and img 3
                    var threeImgMatches = [];
                    //We gotta make it past the first image pair
                    if (lastImgPts && last3DPts && curKeypoints > 0) {
                        console.log('working on the chain');
                        var compPts = allImgPts.secondImgPts;
                        if(!SIFT_LAST_IMG){
                            compPts = allImgPts.firstImgPts;
                        }
                        var curPtIndex = 0;
                        var otherPtIndex = 0;
                        for (curPtIndex = 0; curPtIndex < lastImgPts.length; curPtIndex += 1) {
                            for (otherPtIndex = 0; otherPtIndex < compPts.length; otherPtIndex += 1) {
                                
                                //TODO Might be able to make this a more forgiving equals to allow really close ones to still match
                                if (lastImgPts[curPtIndex].x === compPts[otherPtIndex].x && 
                                    lastImgPts[curPtIndex].y === compPts[otherPtIndex].y) {
                                    
                                    if (last3DPts[curPtIndex]) {
                                        threeImgMatches.push({
                                            firstImgPt: allImgPts.firstImgPts[otherPtIndex],
                                            secondImgPt: allImgPts.secondImgPts[otherPtIndex],
                                            last3DPt: last3DPts[curPtIndex]
                                        });
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    var scale = 0;
                    var matchCount = 0;
                    var threeImgPtIdx;
                    var scales = [];
                    for (threeImgPtIdx = 0; threeImgPtIdx < threeImgMatches.length; threeImgPtIdx += 1) {
                        var curImgMatch = threeImgMatches[threeImgPtIdx];
                        var new3DPt = triangulateLinear(curImgMatch.firstImgPt, curImgMatch.secondImgPt, firstCameraMat, secondCameraMat, invCameraMatrix);
                        if (new3DPt) {
                            var lastDist = matrixOps.getDist(curImgMatch.last3DPt) ;
                            var newDist =  matrixOps.getDist(new3DPt);
                            var distRatio = lastDist/newDist;
                            scale+=distRatio;
                            scales.push({
                                distRatio:distRatio,
                                last3DPt:curImgMatch.last3DPt,
                                new3DPt: new3DPt,
                                lastDist: lastDist,
                                newDist: newDist
                            });
                            matchCount += 1;
                        }
                    }
                    writeOutScales(scales,filePath+MODEL_FOLDER+"\\Scales\\scale"+curKeypoints+".csv");
                    last3DPts = [];
                    if ((!lastImgPts) || scale || curKeypoints === 0) {
                        if (scale) {
                            scale/=matchCount;
                            console.log("scale: " + scale);
                        }
                        //TODO Can do a lot more here to determine a world points actual pixel color
                        var minX, minY, minZ;
                        var maxX, maxY, maxZ;
                        for (curImgPt = 0; curImgPt < allImgPts.firstImgPts.length; curImgPt += 1) {
                            var firstPixelPt = allImgPts.firstImgPts[curImgPt];
                            var secondPixelPt = allImgPts.secondImgPts[curImgPt];
                            var optimalPoint = triangulateLinear(firstPixelPt, secondPixelPt, firstCameraMat, secondCameraMat, invCameraMatrix);
                            var i;
                            if (!optimalPoint) {
                                last3DPts.push(optimalPoint);
                            }
                            if (optimalPoint) {
                                if (scale) {
                                    optimalPoint.x *= scale;
                                    optimalPoint.y *= scale;
                                    optimalPoint.z *= scale;
                                }
                                optimalPoint.firstPixelPt = firstPixelPt;
                                optimalPoint.secondPixelPt = secondPixelPt;
                                last3DPts.push(optimalPoint);
                            }
                        }
                    } else {
                        console.log('could not create a chain of matches to base 2->3 on 1->2');
                    }
                    lastImgPts = allImgPts.secondImgPts;
                    var j,k;
                    var pixDist = 0;
                    var ptDist = 0;
                    for(j = 0; j<lastImgPts.length; j+=1){
                        for(k = 0; k<lastImgPts.length; k+=1){
                            if(j != k){
                                var xDelta = lastImgPts[j].x - lastImgPts[k].x;
                                var yDelta = lastImgPts[j].y - lastImgPts[k].y;
                                pixDist += Math.sqrt(xDelta*xDelta + yDelta*yDelta);
                                xDelta = last3DPts[j].x - last3DPts[k].x;
                                yDelta = last3DPts[j].y - last3DPts[k].y;
                                ptDist += Math.sqrt(xDelta*xDelta + yDelta*yDelta);
                            }
                        }
                    }
                    var pixToPt = ptDist/pixDist;
                    console.log("pixel to world point ratio is "+pixToPt);
                    for(j = 0; j<lastImgPts.length; j+=1){
                        var x = Math.floor(lastImgPts[j].x);
                        var y = Math.floor(lastImgPts[j].y);
                        var regionX, regionY;
                        var realRegionSize = 2 * REGION_SIZE - 1;
                        var imgSize = img.size();
                        for (regionX = 0; regionX < realRegionSize; regionX += 1) {
                            for (regionY = 0; regionY < realRegionSize; regionY += 1) {
                                var curPixelColor = [300, 300, 300]; //blue,green,red
                                var pixX = regionX - Math.floor(realRegionSize / 2);
                                var pixY = regionY - Math.floor(realRegionSize / 2);
                                if (pixX + x < imgSize[1] && pixY + y < imgSize[0] && pixX>=0 && pixY>=0) {
                                    curPixelColor = img.pixel(pixY + y, pixX + x);
                                }
                                var curPt = {x:(pixX+x)*pixToPt,y:(pixY+y)*pixToPt,z:last3DPts[j].z};
                                if (curPt) {
                                    if(curPixelColor[0]<=255){
                                        worldPts.push({
                                            firstPixelPt: lastImgPts[j].firstPixelPt,
                                            secondPixelPt: lastImgPts[j].secondPixelPt,
                                            x: curPt.x,
                                            y: curPt.y,
                                            z: curPt.z,
                                            red: curPixelColor[2],
                                            green: curPixelColor[1],
                                            blue: curPixelColor[0],
                                            zInc: curKeypoints+1
                                        });
                                    }
                                }
                            }
                        }
                    }
                    img.release();
                });
            }
        }
        console.log("well, im making the model...."+worldPts.length);
        //writePointsExcel(worldPts);
        var actualPts = [];
        var j,k;
        var curPlyPtIdx, curX, curPlyPt;
        actualPts = worldPts;
        //This is weird, but potree viewer requires a minimum number of pts
        var minPtsLength = 500;
        if(actualPts.length<minPtsLength){
            var minSizePts = [];
            var curPt = 0;
            while(minSizePts.length<minPtsLength){
                for(curPt = 0; curPt<actualPts.length; curPt+=1){
                    minSizePts.push(actualPts[curPt]);
                }
            }
            actualPts = minSizePts;
        }
        var plyStr = "ply" + '\n';
        plyStr += "format ascii 1.0" + '\n';
        plyStr += "element vertex " + actualPts.length + '\n';
        plyStr += "property float x" + '\n';
        plyStr += "property float y" + '\n';
        plyStr += "property float z" + '\n';
        plyStr += "property uchar red" + '\n';
        plyStr += "property uchar green" + '\n';
        plyStr += "property uchar blue" + '\n';
        plyStr += '\n';
        plyStr += 'end_header' + '\n';
        for (curPlyPt = 0; curPlyPt < actualPts.length; curPlyPt += 1) {
            plyStr += (actualPts[curPlyPt].x * 1000) + ' ';
            plyStr += (actualPts[curPlyPt].y * 1000) + ' ';
            plyStr += (actualPts[curPlyPt].z * 1000) + ' ';
            plyStr += actualPts[curPlyPt].red + ' ';
            plyStr += actualPts[curPlyPt].green + ' ';
            plyStr += actualPts[curPlyPt].blue;
            if (curPlyPt < actualPts.length - 1) {
                plyStr += '\n';
            }
            fs.appendFileSync(filePath + MODEL_FOLDER + '\\' + MODEL_PLY_FILE, plyStr);
            plyStr = "";
        }
        console.log("appended");
        fs.writeFileSync(filePath + MODEL_FOLDER + '\\' + MODEL_FILE, JSON.stringify({
            worldPts: worldPts,
            cameraTranslations: cameraTranslations,
            modelPlyFile: MODEL_PLY_FILE
        }));
        console.log('Finished making the model');
        callback();
    } catch (err) {

    }
}

function writeOutScales(scales,filename){
    var scalesStr = "scale,lastDist,newDist,last.x,last.y,last.z,new.x,new.y,new.z\n";
    var i;
    for(i = 0; i<scales.length; i+=1){
        scalesStr+=scales[i].distRatio+","+scales[i].lastDist+","+scales[i].newDist+",";
        scalesStr+=scales[i].last3DPt.x+","+scales[i].last3DPt.y+","+scales[i].last3DPt.z+",";
        scalesStr+=scales[i].new3DPt.x+","+scales[i].new3DPt.y+","+scales[i].new3DPt.z+"\n";
    }
    fs.writeFileSync(filename,scalesStr);
}

function writePointsExcel(points){
    var i,j;
    var ptsStr = "";
    var pointsColumns = [];
    for(i = 0; i<points.length; i+=1){
        if(points[i].zInc>pointsColumns.length){
            if(pointsColumns.length){
                ptsStr+=",";
            }
            pointsColumns.push([]);
            ptsStr += "zInc,px1.x,px1.y,px2.x,px2.y,x,z,y";
        }
        pointsColumns[points[i].zInc-1].push(points[i]);
    }
    ptsStr+="\n";
    for(i = 0; i<pointsColumns[0].length; i+=1){
        for(j = 0; j<pointsColumns.length; j+=1){
            ptsStr+=pointsColumns[j][i].zInc+","+
            pointsColumns[j][i].firstPixelPt.x+","+
            pointsColumns[j][i].firstPixelPt.y+","+
            pointsColumns[j][i].secondPixelPt.x+","+
            pointsColumns[j][i].secondPixelPt.y+","+
            pointsColumns[j][i].x+","+
            pointsColumns[j][i].z+","+
            pointsColumns[j][i].y;
            if(j<pointsColumns.length-1){
                ptsStr+=","
            }
        }
        ptsStr+="\n";
    }
    console.log("writing out to points...");
    fs.writeFileSync("points.csv",ptsStr);
}

module.exports.makeModel = makeModel;
module.exports.getCameraMatrix = getCameraMatrix;
module.exports.getWorldPoint = getWorldPoint;