var rewire = require('rewire');
var should = require('should');
var assert = require('assert');
var mocha = require('mocha');
var sinon = require('sinon');
var serverApp = rewire('./index.js');

describe('Reading the client secret file for authentication with google drive', function() {
	it('Should quit the whole program when the client secret \
		file is not found', function () {
		var readFileStub = sinon.stub(serverApp.__get__('fs'),'readFileSync');
		readFileStub.throws();
		try{
			serverApp.__get__('readSecretFile')();
			assert.fail('Should have thrown an error',"Did not throw an error");
		}
		catch(err){
		}
		finally{
			readFileStub.restore();
		}
	});
	it('Should begin the authorization process \
		when the secrets file is found', function () {
		var readFileStub = sinon.stub(serverApp.__get__('fs'),'readFileSync');
		readFileStub.returns(JSON.stringify({file:'My file content'}));
		var authFunc = serverApp.__get__('authorize');
		var authSpy = sinon.spy();
		serverApp.__set__('authorize',authSpy);
		serverApp.__get__('readSecretFile')();
		serverApp.__set__('authorize',authFunc);
		readFileStub.restore();
		assert(authSpy.calledOnce);
		assert(authSpy.calledWith({file:'My file content'}));
	});
});
describe('Authorizing using the client secret file with google drive',function(){
	var googleAuthObj,writeFileStub,readFileStub,readlineMock,closeSpy;
	beforeEach(function(done){
		googleAuthObj = serverApp.__get__('googleAuth');
		writeFileStub = sinon.stub(serverApp.__get__('fs'),'writeFile');
		readFileStub = sinon.stub(serverApp.__get__('fs'),'readFile');
		readlineMock = sinon.stub(serverApp.__get__('readline'),'createInterface');
		closeSpy = sinon.spy();
		done();
	});
	it("Should prompt the user to create a token file if it can't be read,\
and it should then error while grabbing the token and not authorize",function(done){
		var authMock = function(){
			this.generateAuthUrl= function(){
				return 'Test auth url';
			};
			this.getToken = function(code,callback){
				callback('test error');
				(serverApp.__get__('isAuthorized')).should.not.be.true();
			};
		};
		serverApp.__set__('googleAuth',function(){
			this.OAuth2= authMock
		});
		readlineMock.returns({
			close : closeSpy,
			question : function(text,callback){
				assert(text); //Show the user some kind of message
				callback('test code');
			}
		});
		serverApp.__get__('authorize')({web:{redirect_uris:[1,2]}});
		readFileStub.yield('error');
		(closeSpy.called).should.be.true();
		done();
	});
	it("Should prompt the user to create a token file if it can't be read,\
and it should then authorize",function(done){
		var authMock = function(){
			this.generateAuthUrl= function(){
				return 'Test auth url';
			};
			this.getToken = function(code,callback){
				callback(undefined,'Test token');
				(serverApp.__get__('isAuthorized')).should.be.true();
				(serverApp.__get__('authClient.credentials')).should.be.exactly('Test token');
			};
		};
		serverApp.__set__('googleAuth',function(){
			this.OAuth2= authMock
		});
		readlineMock.returns({
			close : closeSpy,
			question : function(text,callback){
				assert(text); //Show the user some kind of message
				callback('test code');
			}
		});
		//TODO Make sure the token is actually being written out
		serverApp.__get__('authorize')({web:{redirect_uris:[1,2]}});
		readFileStub.yield('error');
		(closeSpy.called).should.be.true();
		//(writeFileSpy.called).should.be.true();
		done();
	});
	it("Should recognize a token file and just auth off that",function(done){
		var authMock = function(){
		};
		serverApp.__set__('googleAuth',function(){
			this.OAuth2= authMock
		});
		//TODO Make sure the token is actually being written out
		readFileStub.callsArgWith(1,undefined,JSON.stringify({token:'Success Token'}));
		serverApp.__get__('authorize')({web:{redirect_uris:[1,2]}});
		(serverApp.__get__('isAuthorized')).should.be.true();
		(serverApp.__get__('authClient.credentials.token')).should.be.exactly('Success Token');
		//(writeFileSpy.called).should.be.true();
		done();
	});
	afterEach(function(done){
		serverApp.__set__('googleAuth',googleAuthObj);
		writeFileStub.restore();
		readFileStub.restore();
		readlineMock.restore();
		done();
	});
});
describe('Checking online status',function(){
	it('Should execute a request object to google, and set the online status\
to false when there is an error',function(){
		//Mock the request object as a mock
		var reqObj = serverApp.__get__('request');
		var reqMockCalled = false;
		var reqMock = function(url,callback){
			(url).should.be.exactly('http://www.google.com');
			callback('There was an error');
			reqMockCalled  = true;
		};
		serverApp.__set__('request',reqMock);
		var reqSpy = sinon.spy(serverApp.__get__('request'));
		serverApp.__get__('checkOnline')();
		(reqMockCalled).should.be.true();
		(serverApp.__get__('isOnline')).should.be.not.true();
		serverApp.__set__('request',reqObj);

	});
	it('Should execute a request object to google, and set the online status\
to true when it is successful',function(){
		var reqObj = serverApp.__get__('request');
		var reqMockCalled = false;
		var reqMock = function(url,callback){
			(url).should.be.exactly('http://www.google.com');
			callback(undefined);
			reqMockCalled  = true;
		};
		serverApp.__set__('request',reqMock);
		var reqSpy = sinon.spy(serverApp.__get__('request'));
		serverApp.__get__('checkOnline')();
		(reqMockCalled).should.be.true();
		(serverApp.__get__('isOnline')).should.be.true();
		serverApp.__set__('request',reqObj);
	});
});
describe('Reading all files from the cached directory',function(){
	var fsDirStub,fsStatSub,fsReadFileSub;
	beforeEach(function(){
		fsDirStub = sinon.stub(serverApp.__get__('fs'),'readdirSync');
		fsStatSub = sinon.stub(serverApp.__get__('fs'),'statSync');
		fsReadFileSub = sinon.stub(serverApp.__get__('fs'),'readFileSync');
	});
	it('Should add folder objects to the folder stack that pass the dirObj check',function(){
		fsDirStub.returns(['a','b','1','josh','davies','test','as','em','cant']);
		fsStatSub.returns({
			isFile: function(){
				return true;
			}
		});
		//TODO The folderId,folderName,etc could fail if they were legitimately empty (I don't think this an actual issue)
		fsReadFileSub.returns(JSON.stringify({folderId:'ad',
											  folderName:'asdf',
											  videoId:'id',
											  accId:'id',
											  gyroId:'id',
											  vidUrl:'a',
											  accLink:'asdf',
											  gyroLink:'a'}));
		serverApp.__get__('readCachedDirectories')();
		(serverApp.__get__('folders').length).should.be.exactly(9);
	});
	it('Should ignore files with no title file',function(){
		fsDirStub.returns(['a','b','1','josh','davies','test','as','em','cant']);
		fsStatSub.returns({
			isFile: function(){
				return false;
			}
		});
		serverApp.__get__('readCachedDirectories')();
		(serverApp.__get__('folders').length).should.be.exactly(0);
		fsStatSub.returns({
			isFile: function(){
				throw new Error();
			}
		});
		serverApp.__get__('readCachedDirectories')();
		(serverApp.__get__('folders').length).should.be.exactly(0);
		fsStatSub.returns({
			isFile: function(){
				return true;
			}
		});
		fsReadFileSub.returns(JSON.stringify({folderId:'ad',
											  folderName:'asdf',
											  accId:'id',
											  gyroId:'id',
											  vidUrl:'a',
											  accLink:'asdf',
											  gyroLink:'a'}));
		serverApp.__get__('readCachedDirectories')();
		(serverApp.__get__('folders').length).should.be.exactly(0);
	});
	it('Should queue dir objects that have no video file',function(){
		fsDirStub.returns(['a']);
		serverApp.__set__('filesToRead',[]);
		fsStatSub.onFirstCall().returns({
			isFile: function(){
				return true;
			}
		});
		fsStatSub.onSecondCall().returns({
			isFile: function(){
				throw new Error();
			}
		});
		fsStatSub.onCall(2).returns({
			isFile: function(){
				return true;
			}
		});
		fsStatSub.onCall(3).returns({
			isFile: function(){
				return true;
			}
		});
		//TODO The folderId,folderName,etc could fail if they were legitimately empty (I don't think this an actual issue)
		fsReadFileSub.returns(JSON.stringify({folderId:'ad',
											  folderName:'asdf',
											  accId:'id',
											  gyroId:'id',
											  videoId:'video',
											  vidUrl:'a',
											  accLink:'asdf',
											  gyroLink:'b'}));
		serverApp.__get__('readCachedDirectories')();
		(serverApp.__get__('filesToRead').length).should.be.exactly(1);
		(serverApp.__get__('filesToRead')[0].fileId).should.be.exactly('video');
	});
	it('Should queue dir objects that have no acc file',function(){
				fsDirStub.returns(['a']);
		serverApp.__set__('filesToRead',[]);
		fsStatSub.onFirstCall().returns({
			isFile: function(){
				return true;
			}
		});
		fsStatSub.onSecondCall().returns({
			isFile: function(){
				return true;
			}
		});
		fsStatSub.onCall(2).returns({
			isFile: function(){
				return false;
			}
		});
		fsStatSub.onCall(3).returns({
			isFile: function(){
				return true;
			}
		});
		//TODO The folderId,folderName,etc could fail if they were legitimately empty (I don't think this an actual issue)
		fsReadFileSub.returns(JSON.stringify({folderId:'ad',
											  folderName:'asdf',
											  accId:'accId',
											  gyroId:'id',
											  videoId:'video',
											  vidUrl:'a',
											  accLink:'asdf',
											  gyroLink:'b'}));
		serverApp.__get__('readCachedDirectories')();
		(serverApp.__get__('filesToRead').length).should.be.exactly(1);
		(serverApp.__get__('filesToRead')[0].fileId).should.be.exactly('accId');
	});
	it('Should queue dir objects that have no gyro file',function(){
				fsDirStub.returns(['a']);
		serverApp.__set__('filesToRead',[]);
		fsStatSub.onFirstCall().returns({
			isFile: function(){
				return true;
			}
		});
		fsStatSub.onSecondCall().returns({
			isFile: function(){
				return true;
			}
		});
		fsStatSub.onCall(2).returns({
			isFile: function(){
				return true;
			}
		});
		fsStatSub.onCall(3).returns({
			isFile: function(){
				return false;
			}
		});
		//TODO The folderId,folderName,etc could fail if they were legitimately empty (I don't think this an actual issue)
		fsReadFileSub.returns(JSON.stringify({folderId:'ad',
											  folderName:'asdf',
											  accId:'id',
											  gyroId:'gyroId',
											  videoId:'video',
											  vidUrl:'a',
											  accLink:'asdf',
											  gyroLink:'b'}));
		serverApp.__get__('readCachedDirectories')();
		(serverApp.__get__('filesToRead').length).should.be.exactly(1);
		(serverApp.__get__('filesToRead')[0].fileId).should.be.exactly('gyroId');
	});
	afterEach(function(){
		fsDirStub.restore();
		fsStatSub.restore();
		fsReadFileSub.restore();
	});
});
describe('Reading folder names from google drive',function(){
	var driveStub;
	beforeEach(function(){
		driveStub = sinon.stub(serverApp.__get__('google'),'drive');
		serverApp.__set__('foldersToRead',[]);
	});
	it('should do nothing if its not authorized or online',function(){
		serverApp.__set__('isAuthorized',false);
		serverApp.__set__('isOnline',true);
		serverApp.__get__('readFolders')();
		(driveStub.called).should.be.not.true();
		serverApp.__set__('isAuthorized',true);
		serverApp.__set__('isOnline',false);
		serverApp.__get__('readFolders')();
		(driveStub.called).should.be.not.true();
	});
	it('should respond to an query error by exiting',function(){
		serverApp.__set__('isAuthorized',true);
		serverApp.__set__('isOnline',true);
		driveStub.returns({
			files:{
				list:function(obj,callback){
					callback('some kind of error');
				}
			}
		});
		serverApp.__get__('readFolders')();
		(serverApp.__get__('foldersToRead').length).should.be.exactly(0);
	});
	it('should exit on a malformed response',function(){
		serverApp.__set__('isAuthorized',true);
		serverApp.__set__('isOnline',true);
		driveStub.returns({
			files:{
				list:function(obj,callback){
					callback(undefined,{});
				}
			}
		});
		serverApp.__get__('readFolders')();
		(serverApp.__get__('foldersToRead').length).should.be.exactly(0);
	});
	it('should queue folders read successfully',function(){
		serverApp.__set__('isAuthorized',true);
		serverApp.__set__('isOnline',true);
		driveStub.returns({
			files:{
				list:function(obj,callback){
					callback(undefined,{items:[{id:'a',title:'title'},{id:'rain',title:'trey'}]});
				}
			}
		});
		serverApp.__get__('readFolders')();
		(serverApp.__get__('foldersToRead').length).should.be.exactly(2);
	});
	afterEach(function(){
		driveStub.restore();
	});
});
describe('Reading folder objects from google drive',function(){
	var driveStub;
	beforeEach(function(){
		driveStub = sinon.stub(serverApp.__get__('google'),'drive');
	});
	it('Should callback with an auth error when not authorized or online',function(){
		var callbackSpy = sinon.spy();
		serverApp.__set__('isAuthorized',false);
		serverApp.__set__('isOnline',true);
		serverApp.__get__('readDirectory')({},callbackSpy);
		callbackSpy.firstCall.calledWith({},serverApp.__get__('AUTH_ERROR'));
		serverApp.__set__('isAuthorized',true);
		serverApp.__set__('isOnline',false);
		serverApp.__get__('readDirectory')({},callbackSpy);
		callbackSpy.secondCall.calledWith({},serverApp.__get__('AUTH_ERROR'));
	});
	it('Should callback with a query error when files.list method throws an error',function(){
		var callbackSpy = sinon.spy();
		serverApp.__set__('isAuthorized',true);
		serverApp.__set__('isOnline',true);
		driveStub.returns({
			files:{
				list:function(obj,callback){
					callback('some kind of error');
				}
			}
		});
		serverApp.__get__('readDirectory')({},callbackSpy);
		callbackSpy.firstCall.calledWith({},serverApp.__get__('QUERY_ERROR'));
	});
	it('Should exit with a query error when the response object from files.list is malformed',function(){
		var callbackSpy = sinon.spy();
		serverApp.__set__('isAuthorized',true);
		serverApp.__set__('isOnline',true);
		driveStub.returns({
			files:{
				list:function(obj,callback){
					callback(undefined,{});
				}
			}
		});
		serverApp.__get__('readDirectory')({},callbackSpy);
		callbackSpy.firstCall.calledWith({},serverApp.__get__('QUERY_ERROR'));
	
	});
	it('should callback when an object with a video,gyro, and acc file is found',function(){
		var callbackSpy = sinon.spy();
		serverApp.__set__('isAuthorized',true);
		serverApp.__set__('isOnline',true);
		driveStub.returns({
			files:{
				list:function(obj,callback){
					callback(undefined,{items:[
						{title:serverApp.__get__('VIDEO_FILE'),
						 mimeType:'video/mp4',
						 id:'vid',
						 downloadUrl:'out'},
						 {title:serverApp.__get__('ACC_FILE'),
						 mimeType:'application/vnd.google-apps.spreadsheet',
						 id:'acc',
						 exportLinks:{
							 'text/csv': 'sk'
						 }},
						 {title:serverApp.__get__('GYRO_FILE'),
						 mimeType:'application/vnd.google-apps.spreadsheet',
						 id:'gyro',
						 exportLinks:{
							 'text/csv': 'waka'
						 }},
						 {title:'cross',
						 mimeType:'the country'}
					]});
				}
			}
		});
		serverApp.__get__('readDirectory')({},callbackSpy);
		callbackSpy.firstCall.calledWith({},
										 serverApp.__get__('SUCCESS'),
										 'vid',
										 'acc',
										 'gyro',
										 'out',
										 'sk',
										 'waka'
										 );
	
	});
	it('should callback with a directory error when those files are not found',function(){
		var callbackSpy = sinon.spy();
		serverApp.__set__('isAuthorized',true);
		serverApp.__set__('isOnline',true);
		driveStub.returns({
			files:{
				list:function(obj,callback){
					callback(undefined,{items:[
						{title:serverApp.__get__('VIDEO_FILE'),
						 mimeType:'video/mp4',
						 id:'vid',
						 downloadUrl:'out'},
						 {title:serverApp.__get__('ACC_FILE'),
						 mimeType:'application/vnd.google-apps.spreadsheet',
						 id:'acc',
						 exportLinks:{
							 'text/csv': 'sk'
						 }},
						 {title:'cross',
						 mimeType:'the country'}
					]});
				}
			}
		});
		serverApp.__get__('readDirectory')({},callbackSpy);
		callbackSpy.firstCall.calledWith({},serverApp.__get__('DIR_ERROR'));
	});
	afterEach(function(){
		driveStub.restore();
	});
});
describe('Downloading files from google drive',function(){
	var reqObj,reqMock;
	beforeEach(function(){
		reqObj = serverApp.__get__('request');
	});
	it('should callback with a query error when the request fails',function(){
		reqMock = function(obj,callback){
			callback('an error');
		}
		serverApp.__set__('request',reqMock);
		var readFileSpy = sinon.spy();
		serverApp.__get__('readFile')({},readFileSpy);
		readFileSpy.firstCall.calledWith({},serverApp.__get__('QUERY_ERROR'));
	});
	it('should callback with a downloaded file body when successful',function(){
		reqMock = function(obj,callback){
			callback(undefined,'a','body');
		}
		serverApp.__set__('request',reqMock);
		var readFileSpy = sinon.spy();
		serverApp.__get__('readFile')({},readFileSpy);
		readFileSpy.firstCall.calledWith({},serverApp.__get__('SUCCESS'),'body');
	});
	afterEach(function(){
		serverApp.__set__('request',reqObj);
	});
});
describe('Queueing files to download',function(){
	it('should not add a file if the id is already in the queue',function(){
		serverApp.__set__('filesToRead',[{fileId:'drake'}]);
		serverApp.__get__('queueFile')({},'drake','');
		(serverApp.__get__('filesToRead').length).should.be.exactly(1);
	});
	it('should not add a file if it is a video file for a folder obj that already has it',function(){
		serverApp.__set__('folders',[{videoId:'drake',videoFile:'sremmurd'}]);
		serverApp.__set__('filesToRead',[]);
		serverApp.__get__('queueFile')({},'drake','');
		(serverApp.__get__('filesToRead').length).should.be.exactly(0);
	});
	it('should not add a file if it is an acc file for a folder obj that already has it',function(){
		serverApp.__set__('folders',[{accId:'drake',accFile:'sremmurd'}]);
		serverApp.__set__('filesToRead',[]);
		serverApp.__get__('queueFile')({},'drake','');
		(serverApp.__get__('filesToRead').length).should.be.exactly(0);
	});
	it('should not add a file if it is a gyro file for a folder obj that already has it',function(){
		serverApp.__set__('folders',[{gyroId:'drake',gyroFile:'sremmurd'}]);
		serverApp.__set__('filesToRead',[]);
		serverApp.__get__('queueFile')({},'drake','');
		(serverApp.__get__('filesToRead').length).should.be.exactly(0);
	});
	it('should add a file if it passes these checks',function(){
		serverApp.__set__('folders',[{videoId:'drake',videoFile:'sremmurd'}]);
		serverApp.__set__('filesToRead',[]);
		serverApp.__get__('queueFile')({},'else','');
		(serverApp.__get__('filesToRead').length).should.be.exactly(1);
	});
});
describe("Queueing folders to download",function(){
	it('should not add a folder if the folder id is already in the queue',function(){
		serverApp.__set__('foldersToRead',[{folderId:'main'}]);
		serverApp.__get__('queueFolder')('main','ink');
		(serverApp.__get__('foldersToRead').length).should.be.exactly(1);		
	});
	it('should not add a folder if it is already in the folder cache',function(){
		serverApp.__set__('foldersToRead',[]);
		serverApp.__set__('folders',[{folderId:'main'}]);
		serverApp.__get__('queueFolder')('main','ink');
		(serverApp.__get__('foldersToRead').length).should.be.exactly(0);	
	});
	it('should add the folder if it is not cached or queued already',function(){
		serverApp.__set__('foldersToRead',[{folderId:'millions'}]);
		serverApp.__set__('folders',[{folderId:'main'}]);
		serverApp.__get__('queueFolder')('on us','ink');
		(serverApp.__get__('foldersToRead').length).should.be.exactly(2);	
	});
});
describe('Updating directories by pulling them from the queue and downloading them',function(){
	//readDirectory-mock google.drive     .files.list where callback(err,resp) is 2nd param
	//feed it 
	//fsStatSync
	//fsmkdirSync
	//fswriteFileSync
	//queueFile
	it('should skip a folder in the queue if its status is not ready',function(){
		serverApp.__set__('foldersToRead',[{status:'error'},{status:'read'},{status:'reading'}]);
		serverApp.__set__('folders',[]);
		var readDirObj = serverApp.__get__('readDirectory');
		var readDirMock = serverApp.__set__('readDirectory',function(){
			assert.fail();
		});
		serverApp.__get__('updateDirectories')();
		(serverApp.__get__('folders').length).should.be.exactly(0);
		serverApp.__set__('readDirectory',readDirObj);
	});
	it('should reset the folder status when called back with a non-directory error',function(){
		serverApp.__set__('foldersToRead',[{status:'ready'},{status:'ready'},{status:'ready'}]);
		serverApp.__set__('folders',[]);
		var readDirObj = serverApp.__get__('readDirectory');
		var readDirMock = serverApp.__set__('readDirectory',function(folderObj,callback){
			callback(folderObj,serverApp.__get__('QUERY_ERROR'));
		});
		serverApp.__get__('updateDirectories')();
		(serverApp.__get__('folders').length).should.be.exactly(0);
		var foldersToRead = serverApp.__get__('foldersToRead');
		var i;
		for(i = 0; i<foldersToRead.length; i+=1){
			(foldersToRead[i].status).should.be.exactly('ready');
		}
		serverApp.__set__('readDirectory',readDirObj);
	});
	it('should should set folder status to error when called back with a directory error',function(){
		serverApp.__set__('foldersToRead',[{status:'ready'},{status:'ready'},{status:'ready'}]);
		serverApp.__set__('folders',[]);
		var readDirObj = serverApp.__get__('readDirectory');
		var readDirMock = serverApp.__set__('readDirectory',function(folderObj,callback){
			callback(folderObj,serverApp.__get__('DIR_ERROR'));
		});
		serverApp.__get__('updateDirectories')();
		(serverApp.__get__('folders').length).should.be.exactly(0);
		var foldersToRead = serverApp.__get__('foldersToRead');
		var i;
		for(i = 0; i<foldersToRead.length; i+=1){
			(foldersToRead[i].status).should.be.exactly('error');
		}
		serverApp.__set__('readDirectory',readDirObj);
	});
	it('should make a directory for the folder if it does not exist',function(){
		serverApp.__set__('foldersToRead',[{status:'ready'},{status:'ready'},{status:'ready'}]);
		serverApp.__set__('folders',[]);
		var readDirObj = serverApp.__get__('readDirectory');
		var readDirMock = serverApp.__set__('readDirectory',function(folderObj,callback){
			callback(folderObj,serverApp.__get__('SUCCESS'));
		});
		var fsStatStub = sinon.stub(serverApp.__get__('fs'),'statSync');
		var fsMkDirStub = sinon.stub(serverApp.__get__('fs'),'mkdirSync');
		fsStatStub.throws();
		var fsWriteDirStub = sinon.stub(serverApp.__get__('fs'),'writeFileSync');
		serverApp.__get__('updateDirectories')();
		(serverApp.__get__('folders').length).should.be.exactly(3);
		(fsMkDirStub.called).should.be.true();
		serverApp.__set__('readDirectory',readDirObj);
		fsStatStub.restore();
		fsMkDirStub.restore();
		fsWriteDirStub.restore();
	});
	it('should push a directory object onto the folders list,write it to a file, and queue it when successful',function(){
		serverApp.__set__('foldersToRead',[{status:'ready',folderId:'sierra'},
										   {status:'ready',folderId:'leone'},
										   {status:'ready',folderId:'gold'}]);
		serverApp.__set__('folders',[]);
		serverApp.__set__('filesToRead',[]);
		var readDirObj = serverApp.__get__('readDirectory');
		var readDirMock = serverApp.__set__('readDirectory',function(folderObj,callback){
			callback(folderObj,serverApp.__get__('SUCCESS'),
					 'vid','acc','gyro');
		});
		var fsStatStub = sinon.stub(serverApp.__get__('fs'),'statSync');
		var fsMkDirStub = sinon.stub(serverApp.__get__('fs'),'mkdirSync');
		fsStatStub.returns(true);
		var fsWriteDirStub = sinon.stub(serverApp.__get__('fs'),'writeFileSync');
		serverApp.__get__('updateDirectories')();
		(serverApp.__get__('folders').length).should.be.exactly(3);
		(fsMkDirStub.called).should.be.not.true();
		((fsWriteDirStub).calledThrice).should.be.true();
		(serverApp.__get__('filesToRead').length).should.be.exactly(3);
		var i;
		for(i = 0; i<3; i+=1){
			(serverApp.__get__('foldersToRead')[i].status).should.be.exactly('read');
		}
		serverApp.__set__('readDirectory',readDirObj);
		fsStatStub.restore();
		fsMkDirStub.restore();
		fsWriteDirStub.restore();
	});
});