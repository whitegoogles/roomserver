var onlineInterval;
var statusVar = {
	isOnline:false,
	isAuthed:false,
	isServerUp:false
};
$(document).ready(
	function(){
		$('#readingStatusDiv').show();
		$('#readStatusDiv').hide();
		$('#reconnectBtn').hide();
		$('#reconnectBtn').click(function(){
			$('#readingStatusDiv').show();
			$('#readStatusDiv').hide();
			$('#reconnectBtn').hide();
			readStatus();
			onlineInterval = setInterval(readStatus,5000);
		});
		function readStatus(){
			$.ajax(
				'/checkStatus',{
				success: function(resp){
					statusVar.isOnline = resp.isOnline;
					statusVar.isAuthed = resp.isAuthed;	
					statusVar.isServerUp = true;
					setStatus(statusVar);
					$('#reconnectBtn').hide();
				},
				error: function(err){
					console.log(err);
					statusVar.isServerUp = false;
					statusVar.isOnline = false;
					statusVar.isAuthed = false;
					setStatus(statusVar);
					//Kill the interval checker
					clearInterval(onlineInterval);
					//Just make the reconnect button visible	
					$('#reconnectBtn').show();
				},
				timeout:5000 //I don't want calls stacking up...
			}); 
		}
		readStatus();
		onlineInterval = setInterval(readStatus,5000);//Check every 5 seconds
});

function setStatus(status){
	$("#isOnlinePara").removeClass('label-warning');
	$("#isOnlinePara").removeClass('label-success');
	if(status.isOnline){
		$("#isOnlinePara").addClass('label-success');
		$("#isOnlinePara").text('Online');
	}
	else{
		$("#isOnlinePara").addClass('label-warning');
		$("#isOnlinePara").text('Offline');
	}
	$('#isAuthedPara').removeClass('label-warning');
	$('#isAuthedPara').removeClass('label-success');
	if(status.isAuthed){
		$("#isAuthedPara").text('Authenticated');
		$('#isAuthedPara').addClass('label-success');
	}
	else{
		$("#isAuthedPara").text('Authentication failed');
		$('#isAuthedPara').addClass('label-warning');
	}
	
	$('#isServerUpPara').removeClass('label-danger');
	$('#isServerUpPara').removeClass('label-success');
	if(status.isServerUp){
		$("#isServerUpPara").text('Server Connected');
		$("#isServerUpPara").addClass('label-success');
	}
	else{
		$("#isServerUpPara").text('Server offline');
		$("#isServerUpPara").addClass('label-danger');
	}
	$('#readingStatusDiv').hide();
	$('#readStatusDiv').show();
}