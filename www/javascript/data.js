$(document).ready(function(){
	$('#processScratchBtn').click(function(){
		sendProcessReq(curFolder);
	});
	$('#processScreenshotsBtn').click(function(){
		sendProcessReq(curFolder,{
            videoFrames: true
        });
	});
	$('#processKeypointsBtn').click(function(){
		sendProcessReq(curFolder,{
            siftMatches: true
        });
	});
	$('#processEssentialMatricesBtn').click(function(){
		sendProcessReq(curFolder,{
            matrices: true
        });
	});
	$('#processModelBtn').click(function(){
		sendProcessReq(curFolder,{
            model: true
        });
	});
    if(!hasCachedScreenshots){
        $("#processScreenshotsBtn").prop('disabled',true);
    }
    if(!hasCachedKeypoints){
        $("#processKeypointsBtn").prop('disabled',true);
    }
    if(!hasCachedEssentialMatrices){
        $("#processEssentialMatricesBtn").prop('disabled',true);
    }
    if(!hasCachedModel){
        $("#processModelBtn").prop('disabled',true);
    }
    function sendProcessReq(curFolder,cachedObj){
        var urlStr= '/ProcessScreen?'+$.param({curFolder:curFolder});
        location.href = urlStr;
		$.ajax('/convertVideo',{
			data:{
				curFolder:curFolder,
                cached: cachedObj
			}
		});
    }
	function loadTable(url,div){
		var loadHtml = "<img src='/images/gear_loading.gif'>";
		div.html(loadHtml);
		$.ajax(url,{
			success: function(resp){
				var accHtml = '';
				var rows = resp.split('\n');
				if(rows.length){
					var headerVals=rows[0].split(',');
					accHtml+='<table class="table table-bordered">';
					accHtml+='<thead class="thead-default">';
					accHtml+='<tr>';
					var index;
					accHtml+="<th>#</th>"
					for(index = 0; index<headerVals.length; index+=1){
						accHtml+='<th>'+headerVals[index]+'</th>';
					}
					accHtml+='</tr>';
					accHtml+='</thead>';
					accHtml+='<tbody>';
					for(index = 1; index<rows.length; index+=1){
						accHtml +='<tr>';
						accHtml +=('<th scope="row">'+index+'</th>');
						var vals = rows[index].split(',');
						var col;
						for(col = 0; col<headerVals.length; col+=1){
							var colVal = '';
							if(vals[col]){
								colVal = vals[col];
							}
							accHtml+='<td>'+colVal+'</td>';
						}
						accHtml+='</tr>';
					}
					accHtml+='</tbody>';
					accHtml+='</table>';
				}
				div.html(accHtml);
			},
			error: function(err){
				div.html(loadHtml);
				loadTable(url,div);
			}
		});
	}
	loadTable(curFolder.accFile.replace('www/cache','/cache'),$('#accDiv'));
	loadTable(curFolder.gyroFile.replace('www/cache','/cache'),$('#gyroDiv'));
});