$(document).ready(function(){
	$('#imagesDiv').html("<img src='/images/gear_loading.gif'>");
	function loadImages(){
		$.ajax('/cache/'+curFolder.folderId+'/Images/'+'Image',{
			success: function(resp){
				resp = JSON.parse(resp);
				var screenShotsNum = resp.screenShots;
				var screenShotsName = resp.imageNames;
				var screenShotsFolder ='/cache/'+curFolder.folderId+'/Images/';
				var i;
				var imgHtml = "<div id='imageGallery'>";
				for(i = 1; i<=screenShotsNum; i+=1){
					//screenShotsFolder+screenShotsName+"_"+i+resp.imageExt
					imgHtml+='<a href="'+screenShotsFolder+
								screenShotsName+"_"+i+resp.imageExt+'">';
					imgHtml+="<img src='"+screenShotsFolder+'thumb/'+screenShotsName
								+"_"+i+resp.imageExt+"'/>";
					imgHtml+='</a>';
				}
				imgHtml+='</ul>';
				$('#imagesDiv').html(imgHtml);
				$('#imageGallery').lightGallery({
					thumbnail:true
				});
			},
			error: function(err){
				$('#imagesDiv').html("<img src='/images/gear_loading.gif'>");
				loadImages();
			}
		});
	}
	loadImages();
});