$(document).ready(function(){
	$('#imagesDiv').html("<img src='/images/gear_loading.gif'>");
	function loadImages(){
		$.ajax('/cache/'+curFolder.folderId+'/Keypoints/'+'Keypoint',{
			success: function(resp){
				resp = JSON.parse(resp);
				var screenShotsNum = resp.screenShots;
				var screenShotsName = resp.imageNames;
				var screenShotsFolder ='/cache/'+curFolder.folderId+'/Keypoints/';
				var i;
				var imgHtml = "<div id='imageGallery'>";
				for(i = 0; i<screenShotsNum; i+=1){
					imgHtml+='<a href="'+screenShotsFolder+"MatchPic"+i+".png"+'">';
					imgHtml+="<img src='"+screenShotsFolder+'/thumb/MatchPic'+i+".png"+"'/>";
					imgHtml+='</a>';
					imgHtml+='</ul>';
				}
				$('#imagesDiv').html(imgHtml);
				$('#imageGallery').lightGallery({
					thumbnail:true
				});
			},
			error: function(err){
				$('#imagesDiv').html("<img src='/images/gear_loading.gif'>");
				loadImages();
			}
		});
	}
	loadImages();
});