$(document).ready(
	function(){
		$('#goBackBtn').click(function(){
			location.href = '/';
		});
		if(directoryFiles){
			//Add two more of these to pull the excel files, probably use
			//handsontable to show these.
			$.ajax('/readFile',{
				data: {
					file:directoryFiles.videoFile
				},
				success: function(resp){
					if(resp.error){
						console.log(resp.error);
						//$('#status').text('Required file could not be read');
					}
					else{
						$('#videoStatus').text('We read the video file!');
						var videoSource="data:video/mp4;base64,"+resp.file;
						var videoHtml = '<video controls>';
						videoHtml+="<source type='video/mp4' src="+videoSource+">"; //Should I hardcode the type like this?
						videoHtml+="</video>";
						$('#videoHolder').html(videoHtml);
						//This will need to be a button eventually, but lets just write and test the ajax for now
						$.ajax('/convertVideo',
						{
							data:{id:directoryFiles.videoFile.id},	
							success: function(resp){
								
							},
							error: function(err){
								
							}
						});
					}
				},
				error: function(err){
					console.log(err);
					//$('#status').text('Required file could not be read');
				}
			});
		}
		else{
			//Not good, there's no actual directory files object, so we don't know what files to request from our google drive
			//$('#status').text('Missing the actual directory files js object, please return to the main directory using the back button');
		}
	}
);