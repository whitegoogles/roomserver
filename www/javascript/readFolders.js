$(document).ready( 
	function(){
		$('#startProcessBtn').hide();
		$('#startProcessBtn').click(function(){
			if(statusVar.isServerUp && curFolder){
				//Load the data screen
				var urlStr= '/DataScreen?'+$.param({curFolder:curFolder});
				location.href = urlStr;
			}
		});
		readFolders();
		setInterval(readFolders,5000);//Update every five seconds
	}
);
var folders;
var curFolder;
function readFolders(){
	if(statusVar.isServerUp){ //I mean its a global variable... is this that bad?
		console.log('Reading the folders');
		$.ajax('/readFolders',{
			success: function(resp){
				//Find the currently selected one (if there is one)
				var currentSelection = $('#folderListSelect option:selected');
				var selVal;
				if(currentSelection.length){
					//Grab the first one
					selVal = currentSelection.first().val();
					console.log(selVal);
				}
				folders = resp.folders;
				var i,foundSel;
				var listHtml = '';
				if(folders.length){
					var copyCurFolder;
					for(i =0; i<resp.folders.length;i+=1){
						listHtml += '<option>'+resp.folders[i].folderName+'</option>';
						if(resp.folders[i].folderName === selVal){
							foundSel = true;
							copyCurFolder = resp.folders[i];
						}
					}
					$('#folderListSelect').html(listHtml);
					$('#folderListSelect').unbind('change');
					$('#folderListSelect').change(function(){
						onSelectionChange();
					});
					if(!foundSel){
						copyCurFolder = resp.folders[0];
						selVal = $('#folderListSelect option:first').val();
					}
					curFolder = copyCurFolder;
					$('#folderListSelect').val(selVal);
					function onSelectionChange(vidFile){
						if(statusVar.isServerUp){
							var selVal = $('#folderListSelect option:selected').first().val();
							var i,folderObj;
							for(i = 0; i<folders.length; i+=1){
								if(folders[i].folderName == selVal){ //Good, we found it
									folderObj = folders[i];
									curFolder = folders[i];
								}
							}
							//$('video source').attr('src')
							var currentUrl,alreadyLoaded;
							if(folderObj){
								var urlToLoad = '/cache/'+folderObj.folderId+'/Room.mp4';
								if($('#selectedVideo source').length){
									if($('#selectedVideo source').first().attr('src')==urlToLoad){
										alreadyLoaded = true;
									}
								}
							}
							if(!alreadyLoaded){
								$('#startProcessBtn').hide();
								var vidHtml = '';
								if(folderObj){
									//Can we load the video?
									if(folderObj.videoFile){
										vidHtml += "<video id='selectedVideo' controls>";
										vidHtml += "<source src='"+urlToLoad+"' type='video/mp4'>";
										vidHtml += "</video>";
										$('#startProcessBtn').show();
									}
									else{
										vidHtml += "<img src='/images/gear_loading.gif'>";
									}
								}
								else{
									vidHtml += "<img src='/images/gear_loading.gif'>";
								}
								$('#videoDiv').html(vidHtml);
							}
						}
						else{
							curFolder = null;
							console.log('status var is not up');
							$('#startProcessBtn').hide();
							$('#videoDiv').html('');
						}
					}
					onSelectionChange();
				}
				else{
					curFolder = null;
				}
			},
			error: function(err){
				curFolder = null;
				console.log(err);
				console.log('There was an error reading the folders from node');
			},
			timeout: 5000
		});
	}
}